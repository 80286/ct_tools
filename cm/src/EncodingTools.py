class EncodingTools(object):
    # (m, n) -> N
    @staticmethod
    def pi(m, n):
        return 2 ** m * (2 * n + 1) - 1

    # (m, n, p) -> N
    @staticmethod
    def beta(m, n, p):
        pi = EncodingTools.pi

        return pi(pi(m, n), p)

    # (a0, a1, ..., a(k-1)) -> N
    @staticmethod
    def tau(arguments):
        k = len(arguments)
        s = 0

        for i in range(k):
            exponent = sum(arguments[:i + 1]) + i
            s += 2 ** exponent

        return s - 1

    @staticmethod
    def getCodeForCall(call, functions):
        funcCode = int(call[0])
        funcArgc = functions[funcCode][2]
        argCode = 0

        pi = EncodingTools.pi
        beta = EncodingTools.beta

        intFuncId = int(call[1][0])
        intArg1 = int(call[1][1])
        intArg2 = int(call[1][2])

        if funcArgc == 3:
            argCode = beta(intFuncId, intArg1, intArg2)
        elif funcArgc == 2:
            argCode = pi(intFuncId, intArg1)
        elif funcArgc == 1:
            argCode = intFuncId
        else:
            # ?
            pass

        return 4 * argCode + funcCode

    @staticmethod
    def cm_encode_program(calls, functions):
        instructions = []

        for call in calls:
            instructions.append(EncodingTools.getCodeForCall(call, functions))

        print("Instructions: {0}".format(instructions))
        print("Program code: {0}".format(EncodingTools.tau(instructions)))
