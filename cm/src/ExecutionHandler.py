from .ColorConstants import *
from .SystemUtils import *


class ExecutionHandler(object):
    def onNextInstructionCall(self, ctx):
        return False

    def onStart(self, ctx):
        pass

    def onFinish(self, ctx):
        pass


class DefaultExecutionHandler(ExecutionHandler):
    @staticmethod
    def buildColoredStr(s, col, cfg):
        # http://linuxfocus.org/English/May2004/article335.shtml
        if cfg.noColors:
            return s
        else:
            return SystemUtils.getColoredStr(s, col)

    @staticmethod
    def printSeparator(c="=", n=10):
        print(c * n)

    @staticmethod
    def _printFinalResult(ctx):
        argv = ctx.programArgs
        argc = len(argv)
        result = ctx.registry[0]
        title = ctx.title
        msg = ""

        if argc > 1:
            argsTuple = tuple([int(arg) for arg in argv])
            msg = "{0}{1} = {2}".format(title, argsTuple, result)
        elif argc == 1:
            msg = "{0}({1}) = {2}".format(title, argv[0], result)
        else:
            msg = "{0}() = {1}".format(title, result)

        print(msg)

    def _printInstructions(self, ctx):
        self.printSeparator(n=55)
        ctx.programObj._printInstructions(ctx.cfg)
        self.printSeparator(n=55)

    def _printCurrentState(self, ctx):
        colorize = lambda s, col: self.buildColoredStr(s, col, ctx.cfg)
        programObj = ctx.programObj
        ir = int(ctx.ir)

        if ctx.cfg.useZeroCmd:
            i = 0
        else:
            i = 1
            ir = ir + 1

        return programObj.getStateText(i, ir, ctx.cfg.noColors, colorize)

    def _printRegisters(self, ctx, n=10):
        colorize = lambda s, col: self.buildColoredStr(s, col, ctx.cfg)
        colors = ColorConstants
        tab = ctx.registry

        isPrettyPrint = ctx.cfg.printRegistryPretty
        separatorLen = 20 + n * 5

        if isPrettyPrint:
            self.printSeparator("=", separatorLen)

        registers = " | ".join(colorize("%3d" % (tab[i]), colors.green2 if tab[i] == 0 else colors.green) for i in range(n))
        indices = " | ".join(colorize("%3d" % (i), colors.blue) for i in range(n))

        printVector = lambda vec, title, color: print("{0}: | {1}".format(colorize(title, color), vec))
        printVector(registers, "Content", colors.green)
        printVector(indices, "Indices", colors.blue)

        if isPrettyPrint:
            self.printSeparator("=", separatorLen)

    def _doStep(self, ctx):
        cfg = ctx.cfg
        programObj = ctx.programObj
        nextCallStr = "<End of program>"

        if not cfg.noCallMsg:
            idx = ctx.ir if cfg.useZeroCmd else ctx.ir + 1
            programObj.printRuntimeMsg("[{0}][{1}]: Calling '{2}'...".format(ctx.callIdx, idx, ctx.instruction.toStr()))

        if not ctx.isLastInstruction():
            instruction = programObj.instructions[ctx.ir + 1]
            nextCallStr = instruction.toStr()

        print("{0}: [S]kip/[E]nd/[Q]uit/[D]isable step by step/Execute next('{1}')".format(ctx.ir, nextCallStr))

        try:
            cmd = input().lower()

            if cmd == "s":
                self._printInstructions(ctx)
                print("Type call number(0 - {1}): ".format(ctx.irMax))

                dstIdx = int(input())

                ctx.ir = 0 if dstIdx < 0 else dstIdx
                ctx.doContinue = True
            elif cmd == "q":
                ctx.doExit = True
            elif cmd == "e":
                ctx.doBreak = True
            elif cmd == "d":
                cfg.step = False
        except KeyboardInterrupt:
            ctx.doExit = True

        return (ctx.doExit or ctx.doBreak or ctx.doContinue)

    def onStart(self, ctx):
        if ctx.cfg.printOnlyResult:
            return

        self._printInstructions(ctx)

    def onNextInstructionCall(self, ctx):
        programObj = ctx.programObj

        if programObj.isSubfunction:
            return

        cfg = ctx.cfg
        skipIteration = False

        if cfg.clearScreen:
            SystemUtils.doClearScreen()

        if cfg.printRegistry:
            self._printRegisters(ctx)

        if cfg.printRuler:
            programObj.printRuntimeMsg(self._printCurrentState(ctx))

        if cfg.step:
            skipIteration = self._doStep(ctx)
        elif cfg.animationTime != -1:
            SystemUtils.sleep(float(cfg.animationTime))

        return skipIteration

    def onFinish(self, ctx):
        cfg = ctx.cfg

        if ctx.programObj.isSubfunction:
            return

        if not cfg.printOnlyResult:
            self._printRegisters(ctx)

        self._printFinalResult(ctx)
