from .BasicFunctions import *
from .ColorConstants import *
from .ProgramInstruction import *


class FunctionObject(object):
    def __init__(self, name, funcId, argc, func=None):
        self.name = name
        self.funcId = funcId
        self.argc = argc
        self._func = func

    def call(self, ctx, args):
        if not self._func:
            return

        return self._func(ctx, args)


class ProgramObject(object):
    def __init__(self, isSubfunction=False):
        self.isSubfunction = isSubfunction
        self.importedPrograms = {}
        self.functions = []
        self.filename = ""
        self.debug = False
        self.instructions = []
        self.argc = -1

        self.functions = [
        #   (name, id, number of expected arguments, function)
            FunctionObject("Z", 0, 1, BasicFunctions.Z),
            FunctionObject("S", 1, 1, BasicFunctions.S),
            FunctionObject("T", 2, 2, BasicFunctions.T),
            FunctionObject("I", 3, 3, BasicFunctions.I)
        ]
        self.basicFunctionNames = [f.name for f in self.functions[:4]]

    def _printParseErrorMsg(self, lineNum, msg):
        print("{0}:{1}: {2}".format(self.filename, lineNum, msg))

    def _extractInfoFromLineComment(self, comment):
        # Example comments:
        # '# cm: argc=1 title=log'
        # '# cm: import:add.in'

        comment = comment.strip()
        options = comment.split()

        optArgc = len(options)

        if optArgc == 0 or (optArgc > 1 and options[1] != "cm:"):
            return

        for option in options[2:]:
            ec = option.count("=")
            ef = option.find("=")

            if ef == -1:
                option = option.split(":")

                if option[0] == "import" and len(option) == 2:
                    self.importedPrograms[option[1]] = None
                    continue

            if ef == -1 or ec > 1:
                print("Unknown option '{0}'.".format(option))
                continue

            option = option.split("=")

            key = option[0]
            value = option[1]

            if key == "argc":
                self.argc = int(value)
            elif key == "title":
                self.title = value

    def _printAvailableFunctions(self):
        print("[{0}]: Available functions:".format(self.filename))

        for f in self.functions:
            print("[{0}] Function '{1}': {2} arguments".format(f[1], f[0], f[2]))

    def isBasicFunction(self, funcName):
        return funcName in self.basicFunctionNames

    def buildInstructionFromLine(self, line, lineNum):
        lineCommentIdx = line.find("#")
        lineCommentText = ""

        if lineCommentIdx == 0:
            self._extractInfoFromLineComment(line)
            return None
        elif lineCommentIdx == -1:
            line = line.strip()
        else:
            lineCommentText = line[lineCommentIdx + 1:].strip()
            line = line[:lineCommentIdx].strip()

        if not line or line == "":
            return None

        lpIdx = line.find("(", 0)
        rpIdx = line.find(")", lpIdx)

        if lpIdx == -1 or rpIdx == -1 or lpIdx > rpIdx:
            self._printParseErrorMsg(lineNum, "Syntax error at '{0}'".format(line))
            return None

        funcName = line[:lpIdx]
        extraArgs = line[rpIdx + 1:].strip()

        # Building list of arguments:
        argsStr = line[lpIdx + 1 : rpIdx].split(",")
        arguments = [int(s.strip()) for s in argsStr]
        resultRegIdx = None

        if extraArgs and extraArgs != "":
            # NOTE:
            # Old syntax: 'FUNCTION(1, 2 -> 0)' (version: 100215a)
            # New syntax: 'FUNCTION(1, 2) -> 0' (version: 080416a)
            extraArgs = extraArgs.split()

            if len(extraArgs) == 2 and extraArgs[0] == "->":
                # Index of register that saves result of subfunction.
                # Append it to the list of function args:
                resultRegIdx = int(extraArgs[1])
            else:
                self._printParseErrorMsg(lineNum, "Expected '-> [0-9]+', found '{0}'".format(extraArgs))
        
        instruction = ProgramInstruction(funcName, arguments, lineCommentText, resultRegIdx)

        return instruction

    def readFromFile(self, filename):
        self.filename = filename
        lines = None

        with open(filename, "r") as f:
            lines = f.readlines()

        return self.readFromLines(lines)

    def readFromLines(self, lines, append=False):
        if not append:
            self.instructions = []

        lineNum = 0

        for line in lines:
            lineNum += 1

            instruction = self.buildInstructionFromLine(line, lineNum)

            if instruction:
                self.instructions.append(instruction)

    def getFunction(self, functionName):
        for function in self.functions:
            if functionName == function.name:
                return function

        return None

    def printRuntimeMsg(self, msg):
        if self.isSubfunction:
            return

        print(msg)

    def _printInstructions(self, cfg):
        ir = 0

        for instruction in self.instructions:
            msg = "{0}: {1}".format(ir if cfg.useZeroCmd else ir + 1, instruction.toStr())
            self.printRuntimeMsg(msg)
            ir += 1

    def getStateText(self, i, ir, noColors, colorize):
        colors = ColorConstants
        text = ""

        for instruction in self.instructions:
            line = ""

            instructionStr = instruction.toStr()

            # Highlight current line of code:
            if ir == i:
                coloredStr = colorize(instructionStr, colors.green)

                if noColors:
                    line = "{0}: > {1}".format(i, coloredStr)
                else:
                    line = "{0}: {1}".format(i, coloredStr)
            else:
                line = "{0}: {1}".format(i, instructionStr)

            # Print line description:
            lineComment = instruction.lineCommentText

            if len(lineComment) > 0:
                if noColors:
                    line += " # {0}".format(lineComment)
                else:
                    line += colorize(" # {0}".format(lineComment), colors.blue)

            text += line + "\n"
            i += 1

        return text
    
    def isFunctionRegistered(self, name):
        for prog in self.importedPrograms:
            if name == prog.title:
                return True

        return False

    def addFunction(self, f):
        self.functions.append(f)
