class Context(object):
    def __init__(self, cfg, programObj):
        self.registryLen = 100
        self.registry = [0 for _ in range(self.registryLen)]

        self.reset(cfg, programObj, 0, "")

    def reset(self, cfg, programObj, irMax, filename):
        self.ir = 0
        self.irMax = irMax
        self.callIdx = 0

        self.filename = filename
        self.instruction = None
        self.programArgs = []
        self.title = ""

        self.doExit = False
        self.doBreak = False
        self.doContinue = False

        self.programObj = programObj
        self.cfg = cfg

    def resetContinue(self):
        self.doContinue = False

    def isLastInstruction(self):
        return self.ir + 1 == self.irMax

    def skipToInstruction(self, dstInstructionIdx):
        cfg = self.cfg

        if (not cfg.useZeroCmd) and dstInstructionIdx == 0:
            raise Exception("[{0}: {1}] Jump to instruction 0 is not allowed, add -z parameter to use it.".format(self.filename, self.ir))

        targetIdx = dstInstructionIdx if cfg.useZeroCmd else dstInstructionIdx - 1

        if targetIdx >= self.irMax:
            self.doBreak = True
        else:
            self.ir = targetIdx

    def skipToNextInstruction(self):
        self.ir += 1

    def validateInstructionCounter(self):
        if (self.ir if self.cfg.useZeroCmd else self.ir + 1) > self.irMax:
            self.doBreak = True

    def print(self):
        print(f"callIdx: {self.callIdx}; ir: {self.ir}/{self.irMax}; break: {self.doBreak}; continue: {self.doContinue}; exit: {self.doExit}")
