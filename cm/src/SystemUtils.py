import time
import os


class SystemUtils(object):
    @staticmethod
    def doClearScreen():
        os.system("cls" if os.name == "nt" else "clear")

    @staticmethod
    def sleep(numOfSeconds):
        time.sleep(float(numOfSeconds) / 4.0)

    @staticmethod
    def getColoredStr(s, col):
        return '\033[' + str(col[0]) + ';' + str(col[1]) + ";" + str(col[2]) + "m" + s + "\033[0m"
