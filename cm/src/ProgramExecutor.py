from .Context import *
from .SystemUtils import *
from .ColorConstants import *
from .ProgramObject import *

import sys
import os


class ProgramExecutor(object):
    programArgs = []
    filename = ""
    title = ""

    @staticmethod
    def _buildTitle(filename):
        dirSeparator = "/" if os.name == "posix" else "\\"

        lastSlashIdx = filename.rfind(dirSeparator)
        if lastSlashIdx == -1:
            return filename

        title = filename[lastSlashIdx + 1:]

        lastDotIdx = title.rfind(".")
        if lastDotIdx != -1:
            # 'cm_test.in' -> 'cm_test':
            title = title[:lastDotIdx]

        return title

    def __init__(self, cfg, progName):
        self.title = self._buildTitle(progName)
        self.filename = progName
        self.cfg = cfg

        ctx = Context(cfg, None)
        self.ctx = ctx

    def _isExecWithoutArgsBlocked(self, programObj):
        if programObj.argc != -1:
            return

        if programObj.isSubfunction:
            print("Call of subfunction '{0}' terminated. Function expects {1} arguments.".format(self.title, programObj.argc))
            return True

        programObj.printRuntimeMsg("Do you want to continue with no program arguments? [Y/n]?")

        return input() != "Y"

    def _registerSubfunction(self, programObj, newProgramObj, handler):
        dstName = newProgramObj.title

        if programObj.isFunctionRegistered(dstName):
            print("[{0}]: Subfunction {1} already registered.".format(self.filename, dstName))
            return

        def subfunction(ctx, args):
            executor = ProgramExecutor(self.cfg, dstName)
            executor.insertInputArgs(args, newProgramObj.argc)

            result = executor.run(newProgramObj, handler)
            ctx.skipToNextInstruction()

            return result

        f = FunctionObject(dstName, len(programObj.functions), newProgramObj.argc, subfunction)
        programObj.addFunction(f)

        if programObj.debug:
            print("[{0}]: Function '{1}' registered.".format(self.filename, newProgramObj.title))
            programObj._printAvailableFunctions()

    def _loadImportedPrograms(self, programObj, handler):
        importedPrograms = programObj.importedPrograms
        importedProgramNames = list(importedPrograms.keys())

        for name in importedProgramNames:
            if name == self.filename:
                print("[{0}]: Tried to import itself!".format(self.filename))
                continue

            print("[{0}]: Importing {1}'...".format(self.filename, name))
            newProgramObj = ProgramObject(True)
            newProgramObj.readFromFile(name)

            self._registerSubfunction(programObj, newProgramObj, handler)
            importedPrograms[name] = newProgramObj

    def insertInputArgs(self, inputArgs, argc):
        programArgsLen = len(inputArgs)
        self.programArgs = inputArgs
        ctx = self.ctx

        if argc != -1 and programArgsLen != argc:
            msg = "{0} expects {1} argument{2}, found {3}."
            raise Exception(msg.format(self.filename, argc, "" if argc == 1 else "s", programArgsLen))

        # Zero indexed register stores result so we have to skip it by starting from 1:
        firstAllowedIdx = 1

        for argRegIdx, arg in enumerate(inputArgs):
            ctx.registry[firstAllowedIdx + argRegIdx] = int(arg)

    def _resetContext(self, programObj):
        ctx = self.ctx
        ctx.reset(self.cfg, programObj, len(programObj.instructions), self.filename)
        ctx.programArgs = self.programArgs
        ctx.title = self.title

    def run(self, programObj, handler):
        self._loadImportedPrograms(programObj, handler)
        self._resetContext(programObj)
        ctx = self.ctx

        handler.onStart(ctx)

        if self._isExecWithoutArgsBlocked(programObj):
            return

        while True:
            ctx.validateInstructionCounter()

            if ctx.doExit: sys.exit(0)
            if ctx.doBreak: break
            if ctx.doContinue:
                ctx.resetContinue()
                continue

            ctx.callIdx += 1
            ctx.instruction = programObj.instructions[ctx.ir]

            skipIteration = handler.onNextInstructionCall(ctx)
            if skipIteration:
                continue

            ctx.instruction.call(ctx, programObj)
        
        handler.onFinish(ctx)

        return ctx.registry[0]


    @staticmethod
    def loadAndRunProgram(cfg, handler):
        # if cfg_calculatecode:
        #     cm_encode_program(calls)
        #     return

        args = cfg.args
        programArgs = args[1:]

        try:
            programObj = ProgramObject()
            programObj.readFromFile(args[0])

            programExecutor = ProgramExecutor(cfg, args[0])
            programExecutor.insertInputArgs(programArgs, programObj.argc)

            return programExecutor.run(programObj, handler)
        except KeyboardInterrupt:
            print("")
