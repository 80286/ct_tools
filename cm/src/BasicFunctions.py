class BasicFunctions(object):
    @staticmethod
    def Z(ctx, args):
        dstRegIdx = args[0]
        ctx.registry[dstRegIdx] = 0
        ctx.skipToNextInstruction()

    @staticmethod
    def S(ctx, args):
        dstRegIdx = args[0]
        ctx.registry[dstRegIdx] += 1
        ctx.skipToNextInstruction()

    @staticmethod
    def T(ctx, args):
        srcRegIdx = args[0]
        dstRegIdx = args[1]
        ctx.registry[dstRegIdx] = ctx.registry[srcRegIdx]
        ctx.skipToNextInstruction()

    @staticmethod
    def I(ctx, args):
        regIdx1 = args[0]
        regIdx2 = args[1]
        dstInstructionIdx = args[2]

        tab = ctx.registry

        if tab[regIdx1] == tab[regIdx2]:
            ctx.skipToInstruction(dstInstructionIdx)
        else:
            ctx.skipToNextInstruction()
