version = "241121a"

class Config(object):
    def __init__(self):
        self.step = False
        self.noColors = False
        self.printRegistry = True
        self.printRegistryPretty = True
        self.useZeroCmd = False
        self.noCallMsg = False
        self.printRuler = True
        self.printOnlyResult = False
        self.clearScreen = False
        self.calculateMode = False
        self.animationTime = 1
        self.printHelp = False
        self.printVersion = False
        self.unknownArg = None
        self.args = []

    @staticmethod
    def displayHelpScreen(argv):
        print("Arguments:")
        print("\th: Display this screen.")
        print("\ta: Enable animation.")
        print("\ts: Run program step by step.")
        print("\tc: Disable colors.")
        print("\tn: Do not show registers after each iteration.")
        print("\tu: Do not print bar decorations.")
        print("\tz: Allow to use instruction 0.")
        print("\tq: Do not show 'Calling ...' message after each iteration.")
        print("\tr: Do not show source with highlighted current line.")
        print("\to: Call system 'clear' after each iteration (useful with -s).")
        # print("\tk: Calculate code of program.")
        print("\tx: Show only result (quiet mode).")
        print("\tv: Show version.")
        print("Usage: {0} -[vxrzqhscnou]+ program.in program_arg1 program_arg2 ...".format(argv))

    def buildFromArgv(self, argv):
        argc = len(argv)
        self.args = []

        if not argv:
            return False

        for i in range(argc - 1):
            arg = argv[i + 1]

            if arg[0] == "-":
                par = arg[1:]

                for c in par:
                    if c == "h":
                        self.printHelp = True
                    elif c == "s":
                        self.step = True
                    elif c == "c":
                        self.noColors = True
                    elif c == "n":
                        self.printRegistry = False
                    elif c == "u":
                        self.printRegistryPretty = False
                    elif c == "z":
                        self.useZeroCmd = True
                    elif c == "q":
                        self.noCallMsg = True
                    elif c == "r":
                        self.printRuler= False
                    elif c == "x":
                        self.printRegistry = False
                        self.noCallMsg = True
                        self.printRuler = False
                        self.printOnlyResult = True
                        self.animationTime = -1
                    elif c == "o":
                        self.clearScreen = True
                    elif c == "k":
                        self.calculateMode = True
                    elif c == "a":
                        self.animationTime = 1
                        continue
                    elif c == "v":
                        self.printVersion = True
                        return False
                    else:
                        self.unknownArg = c
                        return False
            else:
                self.args.append(arg)

        return True

    def isValid(self, argv):
        if self.unknownArg:
            print("Unknown argument '{0}'".format(self.unknownArg))
            return False

        if self.printVersion:
            print("Version: {0}".format(version))
            return False

        if self.printHelp:
            Config.displayHelpScreen(argv)
            return False

        if not self.args:
            print("Expected program filename.")
            return False
        
        return True
