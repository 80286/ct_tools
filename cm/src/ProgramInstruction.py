class ProgramInstruction(object):
    def __init__(self, funcName, args, lineCommentText, resultRegIdx=None):
        self.funcName = funcName
        self.args = args
        self.lineCommentText = lineCommentText
        self.resultRegIdx = resultRegIdx

    def toStr(self):
        args = self.args
        argc = len(args)

        if argc != 1:
            isSubfunction = (self.resultRegIdx is not None)
            argsStr = "({0})".format(", ".join(str(arg) for arg in args))

            if isSubfunction:
                argsStr += " -> {0}".format(self.resultRegIdx)
        else:
            argsStr = "({0})".format(args[0])

        return "{0}{1}".format(self.funcName, argsStr)

    def _validateArgc(self, filename, func):
        argc = len(self.args)

        if argc != func.argc:
            raise Exception(f"[{filename}] Function '{self.funcName}' expects {func.argc} arguments, found {argc}.")

    def call(self, ctx, programObj):
        isSubfunction = (self.resultRegIdx is not None)
        filename = ctx.filename
        function = programObj.getFunction(self.funcName)

        self._validateArgc(filename, function)

        if not function:
            raise Exception("[{0}]: Function '{1}' not found.".format(filename, self.funcName))

        inputArgs = self.args

        if isSubfunction:
            # Arguments of subfunction call is a list of registry indices so we have to get registry values:
            inputArgs = [ctx.registry[idx] for idx in self.args]

        result = function.call(ctx, inputArgs)

        if isSubfunction:
            # Save result of subfunction call to registry index related to this instruction
            ctx.registry[self.resultRegIdx] = result
