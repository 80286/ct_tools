#!/usr/bin/python3
# -*- coding:utf-8 -*-

from src.ProgramObject import *
from src.ProgramExecutor import *
from src.ExecutionHandler import *
from src.ColorConstants import *
from src.Config import *


def doMain(argv):
    cfg = Config()
    cfg.buildFromArgv(argv)

    if not cfg.isValid(argv):
        return

    handler = DefaultExecutionHandler()
    ProgramExecutor.loadAndRunProgram(cfg, handler)

if __name__ == '__main__':
    doMain(sys.argv)
