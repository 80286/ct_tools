#!/usr/bin/python
# -*- coding:utf-8 -*-

""" Counting Machine Simulator. """

# cm.py
# Copyright (C) 1972 Juzef
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import time
import sys
import os

version = "080416a"

# Configuration variables:
cfg_step = False
cfg_nocolors = False
cfg_printreg = True
cfg_printreg_pretty = True
cfg_usezerocmd = False
cfg_nocallmsg = False
cfg_printruler = True
cfg_onlyresult = False
cfg_clearscreen = False
cfg_calculatecode = False
cfg_animframetime = 1

# Color identifiers:
c_blu = (1, 34, 40)
c_grn = (0, 32, 40)
c_red = (1, 31, 40)
c_gre = (0, 37, 0)
c_gr2 = (1, 37, 40)


def pcol(s, col):
    # http://linuxfocus.org/English/May2004/article335.shtml
    if cfg_nocolors:
        return s
    else:
        return '\033[' + str(col[0]) + ';' + str(col[1]) + ";" + str(col[2]) + "m" + s + "\033[0m"


def cm_bar(c="=", n=10):
    print(c * n)

# {{{

# (m, n) -> N
def cm_pi(m, n):
    return 2 ** m * (2 * n + 1) - 1

# (m, n, p) -> N
def cm_beta(m, n, p):
    return cm_pi(cm_pi(m, n), p)

# (a0, a1, ..., a(k-1)) -> N
def cm_tau(arguments):
    k = len(arguments)
    s = 0

    for i in range(k):
        exponent = sum(arguments[:i + 1]) + i
        # print("TAU: 2^{0}".format(exponent))
        s += 2 ** exponent
    return s - 1

def cm_call_to_code(call):
    function_code = int(call[0])
    function_argc = functions[function_code][2]
    argument_code = 0

    if function_argc == 3:
        argument_code = cm_beta(int(call[1][0]), int(call[1][1]), int(call[1][2]))
    elif function_argc == 2:
        argument_code = cm_pi(int(call[1][0]), int(call[1][1]))
    elif function_argc == 1:
        argument_code = int(call[1][0])
    else:
        # ?
        pass
    # print("Argument_code: {}\nFunction_code: {}\n".format(argument_code, function_code))
    return 4 * argument_code + function_code

def cm_encode_program(calls):
    instructions = []

    for call in calls:
        instructions.append(cm_call_to_code(call))

    print("Instructions: {0}".format(instructions))
    print("Program code: {0}".format(cm_tau(instructions)))

# }}}

##################################################
# 
# CM_Program
# 
##################################################
class CM_Program(object):
    instructions = None
    program_args = None
    filename = ""
    title = ""
    argc = 0

    debug = False

    def f_Z(self, n):
        self.tab[n] = 0

    def f_S(self, n):
        self.tab[n] += 1

    def f_T(self, n, m):
        self.tab[m] = self.tab[n]

    functions = [ # (Name, ID, Amount of arguments, Function)
        ("Z", 0, 1, f_Z),
        ("S", 1, 1, f_S),
        ("T", 2, 2, f_T),
        ("I", 3, 3, None)
    ]
    function_names = []
    basic_function_names = [f[0] for f in functions[:4]]

    # Instruction format:
    # (0:function_name(str), 1:arguments(list), 2:line_comment_text(str))

    # List of registers:
    tab_len = 100
    tab = None

    imported_prog_names = None
    imported_progs = None

    def cut_title(self):
        dir_sep = "/" if os.name == "posix" else "\\"
        # Find first (back)slash:
        slash = self.filename.rfind(dir_sep)

        if slash != -1:
            self.title = self.filename[slash + 1:]
            # print("Slash cut: fname: {0}".format(self.title))
            # Find last dot:
            dot = self.title.rfind(".")
            if dot != -1:
                # Remove part after dot, 'cm_test.in' -> 'cm_test':
                self.title = self.title[:dot]
        else:
            self.title = self.filename

        # print("'{0}' cut to '{1}'.".format(self.filename, self.title))

    """
    ##################################################
        __init__():

    ##################################################
    """
    def __init__(self, filename, subfunction=False):
        self.subfunction = subfunction
        self.filename = filename
        self.imported_prog_names = []
        self.imported_progs = []
        self.instructions = []
        self.tab = [0 for i in range(self.tab_len)]
        # print("CM_Program(filename={0})".format(self.filename))
        self.function_names = [function[0] for function in self.functions]
        self.cut_title()

    def is_basic_function(self, function_name):
        return function_name in self.basic_function_names

    def print_available_functions(self, basic=True):
        print("[{0}]: Available functions:".format(self.filename))
        functions = self.functions if basic else self.functions[3:]

        for f in self.functions:
            print("[{0}] Function '{1}': {2} arguments".format(f[1], f[0], f[2]))

    """ 
    ##################################################
        register_function():

            Register new function name. 
    ##################################################
    """
    def register_subfunction(self, cm_prog):
        func_to_register = cm_prog.title

        for prog in self.imported_progs:
            if func_to_register == prog.title:
                print("[{0}]: Subfunction {1} already registered.".format(self.filename, func_to_register))
                return

        self.imported_progs.append(cm_prog)
        self.function_names.append(func_to_register)
        self.functions.append((func_to_register, len(self.functions), cm_prog.argc, cm_prog))

        if self.debug:
            print("[{0}]: Function '{1}' registered.".format(self.filename, cm_prog.title))
            self.print_available_functions()

    """
    ##################################################
        load_imported_progs():
    
            Imports included sources.
    ##################################################
    """
    def load_imported_progs(self):
        self.imported_progs = []
        # print("[{0}]; imported_prog_names: {1}".format(self.filename, self.imported_prog_names))

        for progname in self.imported_prog_names:
            if progname == self.filename:
                print("[{0}]: Tried to import itself!".format(self.filename))
                continue

            print("[{0}]: Importing {1}'...".format(self.filename, progname))
            new_prog = CM_Program(progname, True)
            new_prog.parse()
            self.register_subfunction(new_prog)


    """
    ##################################################
        instruction_to_str():
    
            Converts instruction to string.
    ##################################################
    """
    def instruction_to_str(self, instruction):
        instruction_args = instruction[1]
        instruction_argc = len(instruction_args)

        if instruction_argc != 1:
            basic_instruction = self.is_basic_function(instruction[0])

            last_arg_index = (instruction_argc) if basic_instruction else (instruction_argc - 1)

            args = instruction_args[:last_arg_index]

            args_str = "("
            args_str += ", ".join(str(arg) for arg in args)
            args_str += ")"

            if not basic_instruction:
                args_str += " -> {0}".format(instruction_args[last_arg_index])
        else:
            args_str = "({0})".format(instruction_args[0])

        return "{0}{1}".format(instruction[0], args_str)

    """ 
    ###################################################
        instructions_to_source():

            Prints compiled instructions to source code.
    ###################################################
    """
    def instructions_to_source(self):
        text = ""
        for instruction in self.instructions:
            text += "{0}\n".format(self.instruction_to_str(instruction))
        return text

    """
    ##################################################
        print_cm_state():
    
            Prints all instructions with highlighted
            current one.
    ##################################################
    """
    def print_cm_state(self, ir):
        text = ""

        if cfg_usezerocmd:
            i = 0
        else:
            i = 1
            ir = ir + 1

        for instruction in self.instructions:
            line = ""

            # Highlight current line of code:
            if ir == i:
                if not cfg_nocolors:
                    line = "{0}: {1}".format(i, pcol(self.instruction_to_str(instruction), c_grn))
                else:
                    line = "{0}: > {1}".format(i, pcol(self.instruction_to_str(instruction), c_grn))
            else:
                line = "{0}: {1}".format(i, self.instruction_to_str(instruction))

            # Print line description:
            if len(instruction[2]) > 0:
                if cfg_nocolors:
                    line += " # {0}".format(instruction[2])
                else:
                    line += pcol(" # {0}".format(instruction[2]), c_blu)

            text += line + "\n"
            i += 1
        return text


    """
    ##################################################
        print_instructions():
    
            Prints source code of program.
    ##################################################
    """
    def print_instructions(self):
        ir = 0
        cm_bar(n=55)

        for instruction in self.instructions:
            self.runtime_msg("{0}: {1}".format(ir if cfg_usezerocmd else ir + 1, self.instruction_to_str(instruction)))
            ir += 1

        cm_bar(n=55)

    """
    ##################################################
        print_registers():
    
            Prints register array.
    ##################################################
    """
    def print_registers(self, n=10):
        tab = self.tab

        bar_len = 10 + n * 5

        # print("[{0}] Registers:".format(self.title))

        if cfg_printreg_pretty:
            cm_bar("=", bar_len)

        register_content = " | ".join(pcol("%2d" % (tab[i]), c_gr2 if tab[i] == 0 else c_grn) for i in range(n))
        register_indexes = " | ".join(pcol("%2d" % (i), c_blu) for i in range(n))

        print("{0}: | {1}".format(pcol("Content", c_grn), register_content))
        print("{0}: | {1}".format(pcol("Indexes", c_blu), register_indexes))

        if cfg_printreg_pretty:
            cm_bar("=", bar_len)

    """
    ##################################################
        parse_error_msg():
    
            Prints message about syntax error to stdout.
    ##################################################
    """
    def parse_error_msg(self, line_num, msg):
        print("{0}:{1}: {2}".format(self.filename, line_num, msg))

    """
    ##################################################
        parse_comment():
    
            Extracts program settings from given line.
    ##################################################
    """
    def parse_comment(self, comment):
        # Local configuration in source code:
        #
        # '# cm: argc=1 title=log'
        # '# cm: import:add.in'

        comment = comment.strip()
        options = comment.split()

        opt_argc = len(options)

        if opt_argc == 0 or (opt_argc > 1 and options[1] != "cm:"):
            return

        for option in options[2:]:
            ec = option.count("=")
            ef = option.find("=")

            if ef == -1:
                option = option.split(":")
                if option[0] == "import" and len(option) == 2:
                    self.imported_prog_names.append(option[1])
                    continue

            if ef == -1 or ec > 1:
                print("Unknown option '{0}'.".format(option))
                continue

            option = option.split("=")

            key = option[0]
            val = option[1]

            if key == "argc":
                self.argc = int(val)
            elif key == "title":
                self.title = val
                # print("New title: {0}".format(self.title))
            else:
                pass

    """
    ##################################################
        get_func_by_name():
    
            Returns function with given name (if exists).
    ##################################################
    """
    def get_func_by_name(self, fun_name):
        for function in self.functions:
            if fun_name == function[0]:
                return function
        return None

    """
    ##################################################
        parse()
    
            Reads source code of CM program.
    ##################################################
    """
    def parse(self, append=False):
        # print("CM_Program.parse(): file: {0}".format(self.filename))
        f = open(self.filename, "r")
        data = f.readlines()

        if not append:
            self.instructions = []

        line_num = 0

        for line in data:
            line_num += 1
            line_comment = line.find("#")
            line_comment_text = ""

            if line_comment == 0:
                # Whole line is commented, parse it and skip to next:
                self.parse_comment(line)
                continue
            elif line_comment == -1:
                # Line has no comments:
                line = line.strip()
            else:
                # Part of line is commented, 
                # store hashed part and continue processing:
                line_comment_text = line[line_comment + 1:].strip()
                line = line[:line_comment].strip()

            if not line or line == "":
                continue

            lp = line.find("(", 0)
            rp = line.find(")", lp)
            if lp == -1 or rp == -1 or lp > rp:
                # Missing parentheses or parentheses mismatch:
                self.parse_error_msg(line_num, "Syntax error at '{0}'".format(line))
                return None

            function_name = line[:lp]
            extra_args = line[rp + 1:].strip()

            # Building list of arguments:
            args_str = line[lp + 1 : rp].split(",")
            arguments = [int(arg_str.strip()) for arg_str in args_str]

            if extra_args and extra_args != "":
                # NOTE:
                # Old syntax: 'FUNCTION(1, 2 -> 0)' (version: 100215a)
                # New syntax: 'FUNCTION(1, 2) -> 0' (version: 080416a)
                extra_args = extra_args.split()

                if len(extra_args) == 2 and extra_args[0] == "->":
                    # extra_args = ("->", "%d")
                    # Index of register to save result of subfunction.
                    # Append it to the list of function args:
                    arguments.append(int(extra_args[1]))
                else:
                    self.parse_error_msg(line_num, "Expected '-> [0-9]+', found '{0}'".format(extra_args))

            self.instructions.append((function_name, arguments, line_comment_text))

        if self.debug:
            print("Parsed instructions:")
            for instr in self.instructions:
                print("{0} ({1})".format(instr[0], instr[1]))

            print("Program '{0}' parsed ({1} lines), {2} instructions found.".format(
                self.filename,
                line_num - 1,
                len(self.instructions)))

    def runtime_msg(self, msg):
        if self.subfunction:
            return
        print(msg)

    """
    ##################################################
        insert_args():

            Inserts given arguments to registers.
    ##################################################
    """
    def insert_args(self, pargs):
        # pargc: number of given arguments
        # argc:  number of required arguments

        pargc = len(pargs)
        self.program_args = pargs

        if self.argc != -1 and pargc != self.argc:
            msg = "{0} expects {1} argument{2}, found {3}."
            print(msg.format(self.filename, self.argc, "" if self.argc == 1 else "s", pargc))
            sys.exit(1)

        # Inserting arguments:
        if pargc > 0:
            argreg = 1
            for parg in pargs:
                self.tab[argreg] = int(parg)
                argreg += 1

    """
    ##################################################
        run():
    
    ##################################################
    """
    def run(self):
        global cfg_step
        tab = self.tab

        # Call counter:
        callnr = 0

        # Instruction register:
        ir = 0
        calls_len = len(self.instructions)

        if not cfg_onlyresult:
            self.print_instructions()

        if self.argc == -1:
            if self.subfunction:
                print("Call of subfunction '{0}' terminated. Function expects {1} arguments.".format(self.title, self.argc))
                return

            runtime_msg("Execute with no program arguments? [Y/n]?")
            ans = raw_input()
            if ans != "Y":
                sys.exit(0)

        while True:
            if cfg_usezerocmd:
                if ir >= calls_len:
                    break
            else:
                if ir + 1 > calls_len:
                    break

            callnr += 1

            call = self.instructions[ir]
            (call_name, call_args, call_argc) = (call[0], call[1], len(call[1]))

            function_name = call[0]
            function = self.get_func_by_name(function_name)

            # print(": Call f:{0} fid: {1} args:{2} args_len:{3}".format(call_name, function[1], call_args, call_argc))

            if function is None:
                # Unknown function name:
                print("[{0}]: Function '{1}' not found.".format(self.filename, function_name))
                return 0

            (function_id, function_argc) = (function[1], function[2])

            if function_id > 3:
                # Call of subfunction stores a result register(in last arg)
                call_argc -= 1

            # print("call: '{0}' f_id: '{1}' args: '{2}'".format(call, function_id, args))

            if call_argc != function_argc:
                # Wrong number of arguments:
                print("[{0}] Function '{1}' expects {2} arguments, found {3}:".format(self.filename, function_name,
                    function_argc,
                    call_argc))
                break

            if not self.subfunction:
                if cfg_clearscreen:
                    os.system('cls' if os.name == 'nt' else 'clear')

                if not cfg_nocallmsg:
                    self.runtime_msg("[{0}][{1}]: Calling '{2}'...".format(callnr, 
                        ir if cfg_usezerocmd else ir + 1, 
                        self.instruction_to_str(call)))

                if cfg_printreg:
                    self.print_registers()

                if cfg_printruler:
                    cm_bar(n=35)
                    self.runtime_msg(self.print_cm_state(ir))
                    # cm_bar(n=35)

                if cfg_step:
                    if ir + 1 == calls_len:
                        next_call = "<End of program>"
                    else:
                        next_call = self.instruction_to_str(self.instructions[ir+1])
                    print("{0}: [S]kip/[E]nd/[Q]uit/[D]isable step by step/Execute next('{1}')".format(ir, next_call))
                    try:
                        cmd = raw_input().lower()
                        if cmd == "s":
                            self.print_instructions(self.instructions)
                            print("Type call number(0 - {1}): ".format(calls_len))
                            in_num = input()
                            if in_num < 0:
                                in_num = 0
                            ir = in_num
                            continue
                        elif cmd == "q":
                            sys.exit(0)
                        elif cmd == "e":
                            break
                        elif cmd == "d":
                            cfg_step = False
                    except KeyboardInterrupt:
                        sys.exit(1)
                        break
                elif cfg_animframetime != -1:
                    time.sleep(float(cfg_animframetime) / 4.0)
            # end if self.subfunction.

            ### Begin execution of instruction. ###
            if function_id == 0:
                self.f_Z(call_args[0])
            elif function_id == 1:
                self.f_S(call_args[0])
            elif function_id == 2:
                self.f_T(call_args[0], call_args[1])
            elif function_id == 3:
                if tab[call_args[0]] == tab[call_args[1]]:
                    if (not cfg_usezerocmd) and call_args[2] == 0:
                        print("[{0}: {1}] Jump to instruction 0 is not allowed, add -z parameter to use it.".format(
                            self.filename, ir))
                        sys.exit(1)

                    target = call_args[2] if cfg_usezerocmd else call_args[2] - 1
                    if target >= calls_len:
                        # Jump to end of program:
                        # print("! target >= calls_len")
                        break
                    else:
                        # print("I({0}, {1}, {2})".format(*args))
                        # print("[{0}][{1}]: Performing jump from {1} to {2}.".format(callnr, 
                        #     ir if cfg_usezerocmd else ir + 1, target))
                        ir = target
                        continue
            else:
                print("Calling subfunction {0}({1})...".format(call_name, call_args))
                # Execute subfunction and get result:
                subf_prog = function[3]
                subf_regs = call_args[:call_argc]
                
                # Now subfunction arguments are the pointers to argument registers,
                # we have to insert values instead of pointers:
                subf_regs_values = []
                for reg in subf_regs:
                    if reg < 0:
                        reg = 0
                    subf_regs_values.append(self.tab[reg])

                subf_prog.insert_args(subf_regs_values)
                tab[call_args[call_argc]] = subf_prog.run()
            ### End execution of instruction ###

            ir += 1

        # end while:
        
        if not self.subfunction and not cfg_onlyresult:
            self.print_registers()

        if not self.subfunction:
            # Print informations about result:
            # filename = self.filename[1]
            # fdot = filename.find(".")
            pargc = len(self.program_args)

            if pargc > 1:
                pa = tuple([int(arg) for arg in self.program_args])
                print("{0}{1} = {2}".format(self.title, pa, tab[0]))
            elif pargc == 1:
                print("{0}({1}) = {2}".format(self.title, self.program_args[0], tab[0]))
            else:
                print("{0}() = {1}".format(self.title, tab[0]))

        return tab[0]


def print_usage():
    print("Usage: {0} -[vxrzqhscnou]+ program.in program_arg1 program_arg2 ...".format(sys.argv[0]))
    sys.exit(1)

def print_help():
    print("Arguments:")
    print("\th: Display this screen.")
    print("\ta: Enable animation.")
    print("\ts: Run program step by step.")
    print("\tc: Disable colors.")
    print("\tn: Do not show registers after each iteration.")
    print("\tu: Do not print bar decorations.")
    print("\tz: Allow to use instruction 0.")
    print("\tq: Do not show 'Calling ...' message after each iteration.")
    print("\tr: Do not show source with highlighted current line.")
    print("\to: Call system 'clear' after each iteration (useful with -s).")
    # print("\tk: Calculate code of program.")
    print("\tx: Show only result (quiet mode).")
    print("\tv: Show version.")
    print_usage()

def main(args):
    # print("args: {0}".format(args))
    cm_prog = CM_Program(args[0])
    
    # if cfg_calculatecode:
    #     cm_encode_program(calls)
    #     return

    program_args = args[1:]

    # if len(program_args) > 0:
    #     print("{0} arguments attached to program: {1}".format(len(program_args), tuple(program_args)))

    try:
        cm_prog.parse()
        cm_prog.insert_args(program_args)
        cm_prog.load_imported_progs()
        cm_prog.run()
    except KeyboardInterrupt, e:
        print("")
        pass

##########################
# Main loop:
##########################
if __name__ == '__main__':
    argc = len(sys.argv)
    program_args = []

    if argc > 1:
        for i in range(argc - 1):
            arg = sys.argv[i + 1]
            if arg[0] == "-":
                par = arg[1:]
                for char in par:
                    if char == "h":
                        print_help()
                    elif char == "s":
                        cfg_step = True
                    elif char == "c":
                        cfg_nocolors = True
                    elif char == "n":
                        cfg_printreg = False
                    elif char == "u":
                        cfg_printreg_pretty = False
                    elif char == "z":
                        cfg_usezerocmd = True
                    elif char == "q":
                        cfg_nocallmsg = True
                    elif char == "r":
                        cfg_printruler = False
                    elif char == "x":
                        cfg_printreg   = False
                        cfg_nocallmsg  = True
                        cfg_printruler = False
                        cfg_onlyresult = True
                        cfg_animframetime = -1
                    elif char == "o":
                        cfg_clearscreen = True
                    elif char == "k":
                        cfg_calculatecode = True
                    elif char == "a":
                        cfg_animframetime = 1 # int(sys.argv[i + 2])
                        continue
                    elif char == "v":
                        print("Version: {0}".format(version))
                        sys.exit(1)
                    else:
                        print("Unknown argument '{0}'".format(char))
                        sys.exit(1)
            else:
                program_args.append(arg)

        if len(program_args) == 0:
            print("Expected program filename.")
            sys.exit(1)
        main(program_args)
    else:
        print_help()
