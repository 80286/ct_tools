Symulator Maszyny Licznikowej
=================

![Screenshot](https://bitbucket.org/80286/ct_tools/raw/master/cm/120116_cm.png)

UWAGA - stary skrypt dla python2 zmienił nazwę z `cm_run.py` na `cm_py2.py`.

Nowa wersja dla *python3* została dodana pod nazwą `cm.py`.

## 1. Struktura programu:
--------------------------
### 1.1 Komentarze ###

Program może zawierać komentarze liniowe rozpoczynające się znakiem `#`
Komentarze mogą zawierać informacje specjalne dla symulatora, rozpoczynające się po prefiksie `cm:`
Przechowują informacje takie jak:  

- nazwa funkcji obliczanej przez program np. `title=Abs`
- ilość argumentów przyjmowanych przez program np. `argc=3`
- importowana podprocedura do programu np. `import:cm_sum.in`

np.

```python
# cm: argc=2 title=Diff
```

lub

```python
# cm: import:cm_pow.in
```

Komentarze stojące w tej samej linii co wywołanie funkcji zostaną zapisane przy odczycie programu
i będą wyświetlane podczas kolejnych iteracji uruchomionego programu.
np. 
```python
I(0, 0, 1) # Goto 1
```

### 1.2 Funkcje ###
Do wyboru mamy 4 podstawowe funkcje:

- `Z(x)`: Zerowanie zawartości rejestru o numerze `x`
- `S(x)`: Inkrementacja zawartości rejestru o numerze `x`
- `T(m, n)`: Podstawienie wartości rejestru `m` w miejsce rejestru `n`.
- `I(m, n, q)`: Jeśli rejestry `m` i `n` mają jednakową zawartość, licznik rozkazów zmieni wartość na `q`.  


### 1.3 Podprocedury ###

Do programu została dodana wstępna obsługa wywoływania podprocedur.

Wywołanie podprocedury odbywa się przez napisanie:

```python
SUBFUNCTION(1, 2) -> 5 # co oznacza, że:
```

* funkcja `SUBFUNCTION` musi zostać najpierw zaimportowana z pliku programu,
należy dodać komentarz importujący plik:
np.

```python
# cm: import:cm_subfunction.in
```

* zaimportowany plik `cm_subfunction` powinien zawierać komentarz z tytułem realizowanej funkcji
np. 

```python
# cm: title=SUBFUNCTION
```

- jeśli plik `cm_subfunction` nie będzie zawierał dyrektywy title, wówczas do funkcji
będzie można się odwołać poprzez nazwę `cm_subfunction`.

## 2. Argumenty wywołania:
--------------------------
### 2.1 Podstawowa składnia ###

* `./cm_run.py -[hascnuzqrokxv]+ cm_program.in arg1 arg2`
* np. `./cm_run.py -x cm_dodaj.py 5 2`

### 2.2 Dostępne argumenty ###

- `h`: Wyświetla ekran pomocy.
- `a`: Włącza tryb animacji - dedykowany dla parametru s lub o.
- `s`: Włącza tryb "Step by step".
- `c`: Wyłącza wyświetlanie kolorów w terminalu.
- `n`: Nie wyświetla rejestrów za każdą iteracją.
- `u`: Nie wyświetla ozdób.
- `z`: Pozwala na użycie instrukcji zerowej (domyślnie pierwsza instrukcja to 1, z myślą o numeracji wierszy w Vim'ie (:set number)).
- `q`: Ogranicza wyjście o napis informujący o wywołaniu konkretnej instrukcji.
- `r`: Nie wyświetla kodu źródłowego po każdej iteracji.
- `o`: Czysci ekran po kazdej iteracji - współpracuje z parametrem o.
- `x`: Uruchomienie programu ogranicza się do wyświetlenia informacji o wyniku.
- `v`: Wyświetla wersję programu.

### 2.1 Najbardziej użyteczne wywołania ###

- `./cm.py -os ./cm_suma.in 50 1` # Czysci ekran i oczekuje potwierdzenia wykonania instrukcji.
- `./cm.py -ao ./cm_suma.in 50 1` # Animuje pracę maszyny (czyszczenie ekranu + odstęp czasu).
- `./cm.py -x ./cm_suma.in 50 1` # Wyświetla tylko wynik
