#!/usr/bin/python3
# -*- coding:utf-8 -*-

from src.ProgramObject import *
from src.ProgramExecutor import *
from src.ExecutionHandler import *
from src.ColorConstants import *
from src.Config import *


def runTests():
    argv = ["test", "-x"]
    cfg = Config()
    cfg.buildFromArgv(argv)

    tests = [
        ("cm_abs.in", [
            ([1, 6], 5),
            ([20, 4], 16)
        ]),
        ("cm_dec.in", [
            ([1], 0),
            ([10], 9)
        ]),
        ("cm_div2.in", [
            ([15], 7),
            ([8], 4)
        ]),
        ("cm_div3.in", [
            ([3], 1),
            ([10], 3)
        ]),
        ("cm_half.in", [
            ([10], 5),
            ([20], 10)
        ]),
        ("cm_max.in", [
            ([3, 5], 5),
            ([5, 3], 5),
            ([10, 1], 10)
        ]),
        ("cm_min.in", [
            ([3, 5], 3),
            ([5, 3], 3),
            ([10, 1], 1)
        ]),
        ("cm_mod.in", [
            ([12, 5], 2),
            ([25, 5], 0)
        ]),
        ("cm_mod2.in", [
            ([12], 0),
            ([25], 1)
        ]),
        ("cm_mul.in", [
            ([12, 2], 24),
            ([4, 8], 32)
        ]),
        ("cm_nsg.in", [
            ([0], 1),
            ([32], 0)
        ]),
        ("cm_pow.in", [
            ([2, 5], 32),
            ([12, 2], 144)
        ]),
        ("cm_sg.in", [
            ([0], 0),
            ([12], 1)
        ]),
        ("cm_sqrt.in", [
            ([25], 5),
            ([16], 4),
            ([18], 4)
        ]),
        ("cm_sum_2x_3y.in", [
            ([5, 3], 19),
            ([15, 7], 51)
        ]),
        ("cm_suma.in", [
            ([5, 3], 8),
            ([15, 7], 22),
            ([15, 1], 16)
        ]),
        # Test of subprocedure call:
        ("cm_test_sub.in", [
            ([5, 3], 9),
        ]),
    ]

    numOfFailedTests = 0
    numOfPassedTests = 0
    handler = ExecutionHandler()

    testIdx = 1
    for (progName, testData) in tests:
        colorize = SystemUtils.getColoredStr
        colors = ColorConstants

        currentNameColor = colors.green2
        subtestIdx = 1
        for inputArgs, expectedResult in testData:
            cfg.args = [progName] + inputArgs
            result = ProgramExecutor.loadAndRunProgram(cfg, handler)

            testStr = colorize(f" [{testIdx}.{subtestIdx}]", colors.blue)
            testStr += colorize(f" {progName}", currentNameColor)
            testStr += f" args: {inputArgs}, expectedResult: {expectedResult} found: {result}"

            msg = None

            if result == expectedResult:
                msg = colorize(f"OK", colors.green) + testStr
                numOfPassedTests += 1
            else:
                msg = colorize(f"FAILED {testStr}", colors.red)
                numOfFailedTests += 1

            print(msg)
            subtestIdx += 1

        testIdx += 1

    print(f"Done. {numOfPassedTests} passed, {numOfFailedTests} failed tests.")

    if numOfFailedTests > 0:
        raise Exception(f"{numOfFailedTests} tests failed. Check logs to check get details.")

if __name__ == '__main__':
    runTests()
