### Narzędzia edukacyjne przeznaczone do pisania programów dla dwóch modeli obliczeniowych w ramach zajęć z Teorii Obliczalności.
==============

#### Działające w trybie tekstowym (python):

- symulator [Maszyny Turinga](https://pl.wikipedia.org/wiki/Maszyna_Turinga) - README jest w katalogu [mt](https://bitbucket.org/80286/ct_tools/src/master/mt/)
- symulator [Maszyny Licznikowej](https://pl.wikipedia.org/wiki/Maszyna_licznikowa) - README jest w katalogu [cm](https://bitbucket.org/80286/ct_tools/src/master/cm/)

#### Graficzne (java):

- niedostatecznie przetestowany symulator [Maszyny Licznikowej](https://pl.wikipedia.org/wiki/Maszyna_licznikowa) napisany w Javie z użyciem biblioteki Swing - jest w katalogu [java](https://bitbucket.org/80286/ct_tools/src/master/java/). Format programów jest zgodny z tym używanym przez wspomniany symulator pythonowy ([cm](https://bitbucket.org/80286/ct_tools/src/master/cm/))
