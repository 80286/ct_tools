package test.java.cm;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import main.java.cm.ProgramParser;
import main.java.cm.Command;

import static org.junit.Assert.*;
import org.junit.Test;

public class ProgramParserTest {
    private ProgramParser parser = new ProgramParser();

    /* Commands: */

    @Test
    public void testCorrectCommand1() throws Exception {
        List <Integer> args = new ArrayList <Integer> ();
        args.add(new Integer(0));
        args.add(new Integer(1));
        args.add(new Integer(2));
        args.add(new Integer(3));

        Command expected = new Command("FUNC", args);
        Command actual = this.parser.parseCommand("FUNC(0, 1, 2, 3)");

        // Does'nt work:
        // assertEquals(expected, actual);
        assertTrue(expected.equals(actual));
    }

    @Test
    public void testCorrectCommand2() throws Exception {
        List <Integer> args = new ArrayList <Integer> ();
        args.add(new Integer(0));

        Command expected = new Command("x", args);
        Command actual = this.parser.parseCommand("x(0)");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void testCorrectCommand3() throws Exception {
        Command expected = new Command("function", new <Integer> ArrayList());
        Command actual = this.parser.parseCommand("function()");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void testCorrectCommand4() throws Exception {
        List <Integer> args = new ArrayList <Integer> ();
        args.add(new Integer(1));
        args.add(new Integer(2));

        Command expected = new Command("subfunction", args);
        expected.setDstRegisterIndex(3);
        Command actual = this.parser.parseCommand("subfunction(1, 2) -> 3");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void testCommandWithoutResultRegisterIndex() throws Exception {
        String expected;

        try {
            this.parser.parseCommand("F(1) ->");
            fail();
        } catch(Exception e){
            expected = String.format(ProgramParser.MSG_ExpectedResultRegisterFoundStr, "->");
            assertEquals(e.getMessage(), expected);
        }
    }

    @Test
    public void testCommandWithoutClosingBracket() throws Exception {
        try {
            this.parser.parseCommand("F(");
            fail();
        } catch(Exception e){
            assertEquals(e.getMessage(), ProgramParser.MSG_ExpectedClosingBracket);
        }
    }

    @Test
    public void testCommandWithoutName() throws Exception {
        try {
            this.parser.parseCommand("(1, 2, 3)");
            fail();
        } catch(Exception e){
            assertEquals(e.getMessage(), ProgramParser.MSG_ExpectedFunctionName);
        }
    }

    @Test
    public void testCommandWithMismatchedBrackets() throws Exception {
        try {
            this.parser.parseCommand("))))xxxx((");
            fail();
        } catch(Exception e){
            assertEquals(e.getMessage(), ProgramParser.MSG_MismatchedBrackets);
        }
    }

    @Test
    public void testWrongCommand1() throws Exception {
        String expected;

        try {
            this.parser.parseCommand("F(1) -");
            fail();
        } catch(Exception e){
            expected = String.format(ProgramParser.MSG_ExpectedResultRegisterFoundStr, "-");
            assertEquals(e.getMessage(), expected);
        }
    }

    /* Line comments: */

    @Test
    public void testEmptyComments() throws Exception {
        assertEquals(this.parser.parseComment(""), null);
        assertEquals(this.parser.parseComment("#"), null);
        assertEquals(this.parser.parseComment("# cccxxxxzzzzzcm:"), null);
    }

    @Test
    public void testCorrectComment1() throws Exception {
        Map <String, String> result;

        result = this.parser.parseComment("# cm: ");
        assertEquals(result, new HashMap <String, String> ());
    }

    @Test
    public void testCorrectComment2() throws Exception {
        Map <String, String> expected = new HashMap <String, String> ();
        Map <String, String> actual = null;

        expected.put("x", "1");
        expected.put("y", "2");
        expected.put("z", "3");

        actual = this.parser.parseComment("# cm: x=1 y=2 z=3");
        assertEquals(expected, actual);
    }

    @Test
    public void testCorrectComment3() throws Exception {
        Map <String, String> expected = new HashMap <String, String> ();
        Map <String, String> actual = null;

        expected.put("x", "1");
        expected.put("y", "2");
        expected.put("z", "3");
        expected.put("import", "file1.inc,file2.inc,f3.inc");

        actual = this.parser.parseComment("# cm: x=1 import:file1.inc y=2 z=3 import:file2.inc import:f3.inc");
        assertEquals(expected, actual);
    }

    @Test
    public void testCorrectComment4() throws Exception {
        Map <String, String> expected = new HashMap <String, String> ();
        Map <String, String> actual = null;

        expected.put("x", "1");
        expected.put("y", "2");
        expected.put("z", "3");
        expected.put("import", "file1.inc,file2.inc");

        actual = this.parser.parseComment("# cm: import:file1.inc,file2.inc x=1 y=2 z=3");
        assertEquals(expected, actual);
    }

    @Test
    public void testWrongComment1() {
        try {
            this.parser.parseComment("# cm: import: file1.inc");
            fail();
        } catch(Exception e){
            assertEquals(e.getMessage(), String.format(ProgramParser.CMT_WrongSettingAtStr, "import:"));
        }
    }

    /* Splitting lines: */

    @Test
    public void testLineSplit() {
        String [] expected = {"F(0, 1, 2)", "# Comment"};

        assertArrayEquals(expected, this.parser.getCommandAndComment("F(0, 1, 2) # Comment"));
        assertArrayEquals(expected, this.parser.getCommandAndComment("F(0, 1, 2)# Comment"));
        assertArrayEquals(expected, this.parser.getCommandAndComment("      F(0, 1, 2)# Comment"));
        assertArrayEquals(expected, this.parser.getCommandAndComment("      F(0, 1, 2)              # Comment      "));
    }
}
