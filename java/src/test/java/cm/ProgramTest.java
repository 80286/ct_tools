package test.java.cm;

import java.util.List;
import java.util.ArrayList;
import main.java.cm.Program;
import main.java.cm.Command;

import static org.junit.Assert.*;
import org.junit.Test;

public class ProgramTest {
    @Test
    public void testUseOfIncrementFunction() throws Exception {
        List <String> code = new ArrayList <String> ();
        code.add("# cm: title=Test argc=0");
        code.add("S(0)");
        code.add("S(0)");

        Program program = new Program();
        program.loadListOfStrings(code);
        program.run();

        assertTrue(program.registers.getResult() == 2);
        assertTrue(program.getTitle().equals("Test"));
        assertTrue(program.getArgc() == 0);
    }

    @Test
    public void testUseOfCopyInstruction() throws Exception {
        List <String> code = new ArrayList <String> ();

        code.add("S(0)");
        code.add("S(0)");
        code.add("T(0, 1)");
        code.add("S(0)");
        code.add("T(0, 2)");

        Program program = new Program();
        program.loadListOfStrings(code);
        program.run();

        assertTrue(program.registers.get(1) == 2 && program.registers.get(2) == 3);
    }

    @Test
    public void testUseOfZeroInstruction() throws Exception {
        List <String> code = new ArrayList <String> ();

        code.add("S(0)");
        code.add("S(0)");
        code.add("S(0)");
        code.add("Z(0)");

        Program program = new Program();
        program.loadListOfStrings(code);
        program.run();

        assertTrue(program.registers.getResult() == 0);
    }

    @Test
    public void testUseOfJumpInstruction() throws Exception {
        List <String> code = new ArrayList <String> ();

        code.add("S(0)");
        code.add("I(0, 0, 5)");
        code.add("S(0)");
        code.add("Z(0)");

        Program program = new Program();
        program.loadListOfStrings(code);
        program.run();

        assertTrue(program.registers.getResult() == 1);
    }

    @Test
    public void testUseOfUnknownInstruction() {
        List <String> code = new ArrayList <String> ();

        code.add("S(0)");
        code.add("S(0)");
        code.add("X(0)");

        Program program = new Program();

        try {
            program.loadListOfStrings(code);
            program.run();
        } catch(Exception e){
            String expected = String.format(Command.MSG_CommandStrNotFound, "X");
            assertTrue(expected.equals(e.getMessage()));
        }
    }

    @Test
    public void testProgram_ModFunction() throws Exception {
        int regA, regB;

        List <String> code = new ArrayList <String> ();
        code.add("S(3)");
        code.add("S(4)");
        code.add("I(2, 3, 6)");
        code.add("I(1, 4, 8)");
        code.add("I(0, 0, 1)");
        code.add("Z(3)");
        code.add("I(0, 0, 4)");
        code.add("T(3, 0)");
        
        List <Integer> args = new ArrayList <Integer> ();
        args.add(regA = 25);
        args.add(regB = 5);

        Program program = new Program();
        program.registers.insertArguments(args);
        program.loadListOfStrings(code);
        program.run();

        assertTrue(program.registers.getResult() == (regA % regB));
    }

    @Test
    public void testProgram_SubFunction1() throws Exception {
        int regA, regB;

        List <String> subprogramCode = new ArrayList <String> ();
        subprogramCode.add("# cm: title=SUM argc=2");
        subprogramCode.add("S(1)");
        subprogramCode.add("S(3)");
        subprogramCode.add("I(2, 3, 5)");
        subprogramCode.add("I(0, 0, 1)");
        subprogramCode.add("T(1, 0)");

        Program subprogram = new Program();
        subprogram.loadListOfStrings(subprogramCode);

        List <String> code = new ArrayList <String> ();
        code.add("# cm: title=SUCC(SUM(x,y)) argc=2");
        code.add("SUM(1, 2) -> 0");
        code.add("S(0)");

        List <Integer> programArgs = new ArrayList <Integer> ();
        programArgs.add(regA = 25);
        programArgs.add(regB = 5);

        Program program = new Program();
        program.registers.insertArguments(programArgs);
        program.loadListOfStrings(code);
        program.importSubprogram(subprogram);
        program.linkCommandsWithFunctions();
        program.run();

        assertTrue(program.registers.getResult() == (regA + regB + 1));
    }
}
