package main.java.cm;

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;

import main.java.common.ListOfStrings;
import main.java.cm.BasicFunctions.*;

public class Program {
    /** Number of registers. */
    private static final int MEMSIZE = 32;

    /** Array of registers. */
    public Memory registers;

    /** Default parser. */
    private CMParser parser = new ProgramParser();

    /** Path of processed file. */
    private String path;

    /** HashMap with settings stored in line comments. */
    private Map <String, String> properties;

    /** Number of currently processed line. */
    private int currentLine;

    /** List of pairs (functionName, functionArgs). */
    private List <Command> commands;

    /** List of available functions. */
    private Map <String, Function> functions = null;

    /** List of subprograms. */
    private List <Program> importedPrograms = null;

    /** Index of currently executed command. */
    private int commandCounter;

    public Program() {
        this.properties = new HashMap <String, String> ();
        this.commands = new ArrayList <Command> ();
        this.registers = new Memory(MEMSIZE);
        this.importedPrograms = new ArrayList <Program> ();
        this.registerBasicFunctions();
    }

    private void registerBasicFunctions() {
        this.functions = new HashMap <String, Function> ();

        this.functions.put("Z", new F_RegisterZero());
        this.functions.put("S", new F_RegisterIncrement());
        this.functions.put("T", new F_RegisterCopy());
        this.functions.put("I", new F_InstructionJump());
    }

    public void linkCommandsWithFunctions() throws Exception {
        for(Command command: this.commands) {
            command.linkWithProgram(this);
        }
    }

    private String [] getArrayOfImportedFilenames() {
        String importedFilenames;

        if ((importedFilenames = this.getProperty("import")) == null) {
            return null;
        }

        return importedFilenames.split(",");
    }

    public List <Integer> evalRegisterIndexes(List <Integer> regIndexes) {
        List <Integer> regValues;

        regValues = new ArrayList <Integer> ();

        for(Integer regIndex: regIndexes) {
            regValues.add(new Integer(registers.get(regIndex.intValue())));
        }

        return regValues;
    }

    public void registerSubfunction(Program subprogram) {
        this.functions.put(subprogram.getTitle(), new Function() {
            private Program subprogram = null;

            public Function init(Program subprogram) {
                this.subprogram = subprogram;
                this.setExpectedNumberOfArgs(subprogram.getArgc());
                return this;
            }

            public void call(Program program, List <Integer> args, int dstRegIndex) throws Exception {
                int result;

                this.subprogram.registers.insertArguments(program.evalRegisterIndexes(args));
                this.subprogram.run();

                result = this.subprogram.registers.getResult();

                program.registers.set(dstRegIndex, result);
                program.gotoNextCommand();
            }
        }.init(subprogram));
    }

    public void importSubprogram(Program subprogram) {
        this.importedPrograms.add(subprogram);
        this.registerSubfunction(subprogram);
    }

    public void importSubprograms() throws Exception {
        String [] arrayOfFilenames;
        Program subprogram;

        if ((arrayOfFilenames = this.getArrayOfImportedFilenames()) == null) {
            return;
        }

        for(String filename: arrayOfFilenames) {
            subprogram = new Program();
            subprogram.loadFromFile(filename);
            this.importSubprogram(subprogram);
        }
    }

    public Function getFunctionByName(String name) throws Exception {
        return this.functions.get(name);
    }

    /**
     * Builds formatted message with information about processing state.
     *
     * @param msg
     *      String with message.
     *
     * @return
     *      Formatted message.
     */
    private String getReaderMessage(String msg) {
        return String.format("%s: %d: %s", this.getFilename(), this.currentLine, msg);
    }

    public String getProperty(String key) {
        return this.properties.get(key);
    }

    /**
     * Parses command and stores it in list of commands.
     *
     * @param command
     *      String with call of function.
     * @param comment
     *      Additional line comment.
     * @throws Exception
     *      If command has wrong syntax.
     */
    private void addCommand(String command, String comment) throws Exception {
        Command parsedCommand;

        parsedCommand = this.parser.parseCommand(command);
        parsedCommand.setComment(comment);

        this.commands.add(parsedCommand);
    }

    private void execLine(String line) throws Exception {
        Map <String, String> mapOfSettings;
        boolean isSpecialComment;
        String [] lineParts;
        String comment;
        String command;

        lineParts = this.parser.getCommandAndComment(line);

        command = lineParts[0];
        comment = lineParts[1];

        mapOfSettings = null;
        if (comment != null && !comment.isEmpty()) {
            mapOfSettings = this.parser.parseComment(comment);
            if (mapOfSettings != null) {
                this.properties.putAll(mapOfSettings);
            }
        }

        isSpecialComment = (mapOfSettings != null);

        if (command != null && !command.isEmpty()) {
            this.addCommand(command, isSpecialComment ? null : comment);
        }
    }

    public void clearPreviousData() {
        this.commands.clear();
        this.properties.clear();
        this.importedPrograms.clear();
        this.registerBasicFunctions();
    }

    public void loadListOfStrings(List <String> lines) throws Exception {
        this.clearPreviousData();
        this.currentLine = 0;

        for(String line: lines) {
            if (line.isEmpty()) {
                continue;
            }

            try {
                this.execLine(line);
            } catch(NullPointerException e) {
                e.printStackTrace();
            } catch(Exception e) {
                throw new Exception(this.getReaderMessage(e.getMessage()));
            }

            this.currentLine++;
        }
    }

    public void loadFromFile(String path) throws Exception {
        List <String> code;
        
        code = ListOfStrings.buildFromFile(path);
        this.loadListOfStrings(code);
    }

    public void resetCommandCounter() {
        this.commandCounter = 0;
    }

    public int getNumberOfInstructions() {
        return this.commands.size();
    }

    public boolean hasNextCommand() {
        return (this.commandCounter < this.commands.size());
    }

    public void gotoToCommand(int commandIndex) {
        this.commandCounter = commandIndex - 1;
    }

    public void gotoNextCommand() {
        this.commandCounter++;
    }

    public int getCommandCounter() {
        return this.commandCounter;
    }

    public Command getCurrentCommand() {
        return this.commands.get(this.commandCounter);
    }

    public void executeNextCommand() throws Exception {
        this.getCurrentCommand().run(this);
    }

    public void doPreRunAction() throws Exception {
        this.importSubprograms();
        this.linkCommandsWithFunctions();
        this.resetCommandCounter();
    }

    public void run() throws Exception {
        this.doPreRunAction();

        while(this.hasNextCommand()) {
            this.executeNextCommand();
        }
    }

    public void setPath(String newPath) {
        this.path = newPath;
    }

    public static String getBasenameOfPath(String path) {
        return path.substring(path.lastIndexOf(File.separator) + 1, path.length());
    }

    public String getFilename() {
        String title;

        if (this.path != null) {
            return getBasenameOfPath(this.path);
        }
        
        return "Unnamed";
    }

    public String getTitle() {
        return this.properties.get("title");
    }

    public int getArgc() {
        String argc;

        if ((argc = this.properties.get("argc")) == null) {
            return -1;
        } else {
            return Integer.parseInt(argc);
        }
    }

    public List <String> getFormattedCode() {
        List <String> result;
        String lastComment;

        result = new ArrayList <String> ();

        for(Command command: this.commands) {
            result.add(command.getCode());
        }

        if (!this.properties.isEmpty()) {
            lastComment = "# cm: ";

            for(Map.Entry <String, String> entry: this.properties.entrySet()) {
                lastComment += String.format("%s=%s ", 
                        entry.getKey(), 
                        entry.getValue());
            }

            result.add(System.lineSeparator() + lastComment);
        }

        return result;
    }
}
