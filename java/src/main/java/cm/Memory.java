package main.java.cm;

import java.lang.Integer;
import java.util.List;

public class Memory {
    private int [] registers;

    public Memory(int numberOfRegisters) {
        this.registers = new int[numberOfRegisters];
        this.clear();
    }

    public void clear() {
        for (int i = 0; i < this.registers.length; i++) {
            this.registers[i] = 0;
        }
    }

    public int getNumberOfRegisters() {
        return this.registers.length;
    }

    public int getResult() {
        return this.registers[0];
    }

    public int get(int registerIndex) {
        return this.registers[registerIndex];
    }

    public void set(int registerIndex, int value) {
        this.registers[registerIndex] = value;
    }

    public void insertArguments(List <Integer> args) {
        int registerIndex = 1;

        for(Integer arg: args) {
            this.registers[registerIndex] = arg.intValue();
            registerIndex++;
        }
    }
}
