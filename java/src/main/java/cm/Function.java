package main.java.cm;

import java.lang.Integer;
import java.lang.Exception;
import java.util.List;

public abstract class Function {
    private int expectedNumberOfArgs;

    protected void setExpectedNumberOfArgs(int numberOfArgs) {
        this.expectedNumberOfArgs = numberOfArgs;
    }

    public void call(Program program, List <Integer> args, int resultRegIndex) throws Exception {
        if (args.size() != this.expectedNumberOfArgs) {
            throw new Exception(String.format("Function expected %d arguments, %d given.",
                        this.expectedNumberOfArgs,
                        args.size()));
        }
    }
}
