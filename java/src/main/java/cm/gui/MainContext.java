package main.java.cm.gui;

import main.java.cm.Program;

public class MainContext {
    private MainPanelController mainPanelController;

    private MemoryPanelController memoryPanelController;
    private SourcePanelController sourcePanelController;
    private ControlPanelController controlPanelController;

    private Program program;

    public MainContext(MainPanelController parentClassOfContext) {
        this.program = new Program();
        this.mainPanelController = parentClassOfContext;
        this.memoryPanelController = new MemoryPanelController(this);
        this.sourcePanelController = new SourcePanelController(this);
        this.controlPanelController = new ControlPanelController(this);
    }

    public MainPanelController getMainPanelController() {
        return this.mainPanelController;
    }

    public MemoryPanelController getMemoryPanelController() {
        return this.memoryPanelController;
    }

    public SourcePanelController getSourcePanelController() {
        return this.sourcePanelController;
    }

    public ControlPanelController getControlPanelController() {
        return this.controlPanelController;
    }

    public Program getProgram() {
        return this.program;
    }
}
