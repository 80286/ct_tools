package main.java.cm.gui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import main.java.cm.Program;
import main.java.cm.Memory;

public class MemoryPanelController {
    private MemoryPanelView view;
    private MainContext mainContext;

    public MemoryPanelController(MainContext context) {
        this.view = new MemoryPanelView();
        this.mainContext = context;
    }

    public MemoryPanelView getView() {
        return this.view;
    }

    public void saveValuesToMemory() {
        JSpinner [] spinners;
        Memory registers;

        registers = this.mainContext.getProgram().registers;
        spinners = this.view.getSpinners();

        for (int i = 0; i < spinners.length; i++) {
            registers.set(i, ((Integer)spinners[i].getValue()).intValue());
        }
    }

    public void updateSpinners() {
        JSpinner [] spinners;
        Memory registers;
        int oldValue;
        int newValue;

        registers = this.mainContext.getProgram().registers;
        spinners = this.view.getSpinners();

        for (int i = 0; i < spinners.length; i++) {
            oldValue = ((Integer)spinners[i].getValue()).intValue();
            newValue = registers.get(i);

            if (oldValue != newValue) {
                spinners[i].setValue(registers.get(i));
            }
        }
    }

    public void resizeTo(int newSize) {
        this.view.removeAll();
        this.view.add(new JScrollPane(this.view.buildCellPanel(newSize)));
        this.view.revalidate();
        this.view.repaint();
    }
}
