package main.java.cm.gui;

import javax.swing.text.DefaultHighlighter;
import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import java.util.List;
import java.util.ArrayList;

import main.java.common.TextLineNumber;

public class SourcePanelController {
    private MainContext mainContext;
    private SourcePanelView view;

    public SourcePanelController(MainContext context) {
        this.mainContext = context;
        this.view = new SourcePanelView();
        this.view.addTab(context.getProgram().getFilename());
    }

    public SourcePanelView getView() {
        return this.view;
    }

    public void setTitleOfCurrentTab(String newTitle) {
        JTabbedPane tabbedPane;
        int currentTabIndex;

        tabbedPane = this.view.getTabbedPane();
        currentTabIndex = tabbedPane.getSelectedIndex();
        tabbedPane.setTitleAt(currentTabIndex, newTitle);
    }

    public JTextArea getTextAreaOfCurrentTab() {
        JScrollPane selectedScrollPane;
        JTextArea selectedTextPane;
        JTabbedPane tabbedPane;
        JViewport viewport;

        tabbedPane = this.view.getTabbedPane();
        selectedScrollPane = (JScrollPane)tabbedPane.getSelectedComponent();
        viewport = selectedScrollPane.getViewport();
        selectedTextPane = (JTextArea)viewport.getView();

        return selectedTextPane;
    }

    public void clearCurrentPanel() {
        JTextArea currentTextArea;

        currentTextArea = this.getTextAreaOfCurrentTab();
        currentTextArea.setText("");
    }

    public List <String> getListOfStrings() {
        JTextArea currentTextArea;
        List <String> listOfLines;
        String currentTextAreaContent;
        String [] lines;

        currentTextArea = this.getTextAreaOfCurrentTab();
        currentTextAreaContent = currentTextArea.getText();

        listOfLines = new ArrayList <String> ();
        lines = currentTextAreaContent.split("\\r?\\n");

        for(String line: lines) {
            listOfLines.add(line);
        }

        return listOfLines;
    }

    public void setListOfStrings(List <String> lines) {
        JTextArea currentTextArea;
        String newText;

        newText = "";
        for(String line: lines) {
            newText += (line + System.lineSeparator());
        }

        currentTextArea = this.getTextAreaOfCurrentTab();
        currentTextArea.setText(newText);
    }

    public static void highlightRowOfTextArea(JTextArea textArea, int rowIndex, Color color) {
        DefaultHighlighter.DefaultHighlightPainter painter;
        int startOffset;
        int endOffset;

        try {
            startOffset = textArea.getLineStartOffset(rowIndex);
            endOffset = textArea.getLineEndOffset(rowIndex);

            painter = new DefaultHighlighter.DefaultHighlightPainter(color);
            textArea.getHighlighter().addHighlight(startOffset, endOffset, painter);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void highlightRowOfCurrentTextArea(int rowIndex, Color color) {
        JTextArea currentTextArea;
        SourcePanelController sourcePanel;

        sourcePanel = this.mainContext.getSourcePanelController();
        currentTextArea = this.getTextAreaOfCurrentTab();
        sourcePanel.highlightRowOfTextArea(currentTextArea, rowIndex, color);
    }

    public void highlightError(int rowIndex) {
        this.highlightRowOfCurrentTextArea(rowIndex, Color.RED);
    }

    public void highlightExecutedLine(int rowIndex) {
        this.highlightRowOfCurrentTextArea(rowIndex, Color.GRAY);
    }

    public void removeCurrentTextAreaHighlights() {
        JTextArea currentTextArea;

        currentTextArea = this.getTextAreaOfCurrentTab();
        currentTextArea.getHighlighter().removeAllHighlights();
    }

    public void disableCurrentTextArea() {
        JTextArea currentTextArea;

        currentTextArea = this.getTextAreaOfCurrentTab();
        currentTextArea.setEditable(false);
    }

    public void enableCurrentTextArea() {
        JTextArea currentTextArea;

        currentTextArea = this.getTextAreaOfCurrentTab();
        currentTextArea.setEditable(true);
    }
}
