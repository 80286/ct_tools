package main.java.cm.gui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import main.java.cm.Program;
import main.java.cm.Memory;

public class MemoryPanelView extends JPanel {
    private JSpinner [] spinners;
    private JLabel [] labels;
    private JScrollPane pane;

    public MemoryPanelView() {
        this.setBorder(BorderFactory.createTitledBorder("MemoryPanel"));
        this.add(new JScrollPane(this.buildCellPanel(12)));
    }

    private JLabel buildCellLabel(Dimension cellSize, int labelIndex) {
        JLabel label;

        label = new JLabel(String.format("%d", labelIndex));
        label.setPreferredSize(cellSize);
        label.setHorizontalAlignment(JLabel.CENTER);

        return label;
    }

    private JSpinner buildCellSpinner(Dimension cellSize) {
        JSpinner spinner;

        spinner = new JSpinner(new SpinnerNumberModel(0, 0, 9999, 1));
        spinner.setPreferredSize(cellSize);
        ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setHorizontalAlignment(JLabel.CENTER);

        return spinner;
    }

    public JPanel buildCellPanel(int numberOfCells) {
        Dimension cellSize;
        JPanel panel;

        this.labels = new JLabel[numberOfCells];
        this.spinners = new JSpinner[numberOfCells];

        panel = new JPanel();
        panel.setLayout(new GridLayout(2, 0));

        cellSize = new Dimension(48, 24);

        for(int i = 0; i < numberOfCells; i++) {
            this.labels[i] = this.buildCellLabel(cellSize, i);
            panel.add(this.labels[i]);
        }

        for(int i = 0; i < numberOfCells; i++) {
            this.spinners[i] = this.buildCellSpinner(cellSize);
            panel.add(this.spinners[i]);
        }

        return panel;
    }

    public JSpinner [] getSpinners() {
        return this.spinners;
    }

    public JLabel [] getLabels() {
        return this.labels;
    }
}
