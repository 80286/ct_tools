package main.java.cm.gui;

import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import java.awt.Dimension;

import main.java.cm.Program;

public class MainPanelController {
    private MainPanelView view;
    private MainContext mainContext;

    public MainPanelController() {
        this.mainContext = new MainContext(this);
        this.view = new MainPanelView(this.mainContext);
        this.view.showJFrame();
    }

    public MainPanelView getView() {
        return this.view;
    }

    public void setMessage(String message) {
        JLabel msgLabel;

        msgLabel = this.view.getMessageLabel();
        msgLabel.setText(message);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainPanelController();
            }
        });
    }
}
