package main.java.cm.gui;

import java.awt.GridLayout;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.BoxLayout;
import javax.swing.SpringLayout;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollPane;

import java.util.List;
import java.util.ArrayList;

import main.java.common.ListOfStrings;
import main.java.common.LabeledJSpinner;

public class ControlPanelView extends JPanel {
    private List <LabeledJSpinner> spinners;
    private List <JButton> buttons;

    public List <LabeledJSpinner> getSpinners() {
        return this.spinners;
    }

    public List <JButton> getButtons() {
        return this.buttons;
    }

    public ControlPanelView() {
        this.setBorder(BorderFactory.createTitledBorder("ControlPanel"));

        this.buttons = this.buildListOfButtons();
        this.spinners = this.buildListOfSpinners();

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(this.buildButtonPanel());
        this.add(this.buildOptionsPanel());
    }

    private JPanel buildBorderedComponent(String title, JComponent component) {
        JPanel panel;
        
        panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder(title));
        panel.add(component);

        return panel;
    }

    private LabeledJSpinner buildIntegerSpinner(String label, int v, int min, int max, int step) {
        LabeledJSpinner spinner;

        spinner = new LabeledJSpinner(label);
        spinner.setModel(new SpinnerNumberModel(v, min, max, step));

        return spinner;
    }

    private List <JButton> buildListOfButtons() {
        List <JButton> buttons;

        buttons = new ArrayList <JButton> ();
        buttons.add(new JButton("Run"));
        buttons.add(new JButton("Stop"));
        buttons.add(new JButton("Load"));
        buttons.add(new JButton("Save"));
        buttons.add(new JButton("ClearRegisters"));
        buttons.add(new JButton("ClearCode"));
        buttons.add(new JButton("Step"));

        return buttons;
    }

    private List <LabeledJSpinner> buildListOfSpinners() {
        List <LabeledJSpinner> spinners;

        spinners = new ArrayList <LabeledJSpinner> ();
        spinners.add(this.buildIntegerSpinner("NumOfRegisters", 12, 4, 16, 1));
        spinners.add(this.buildIntegerSpinner("TimeInterval", 500, 0, 2000, 100));

        return spinners;
    }

    private JPanel buildButtonPanel() {
        int numberOfColumns;
        JPanel buttonPanel;
        JPanel row;

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

        for(int i = 0; i < this.buttons.size(); i += 2) {
            row = new JPanel();
            row.setLayout(new GridLayout(0, 2));
            row.setMaximumSize(new Dimension(320, 64));

            numberOfColumns = (i + 1 == this.buttons.size()) ? 1 : 2;

            for(int j = 0; j < numberOfColumns; j++) {
                row.add(this.buttons.get(i + j));
            }

            buttonPanel.add(row);
        }

        return buttonPanel;
    }

    public JPanel buildOptionsPanel() {
        JPanel optionsPanel;

        optionsPanel = new JPanel();
        // optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));

        for(LabeledJSpinner spinner: this.spinners) {
            optionsPanel.add(spinner.toJPanel());
        }

        return optionsPanel;
    }
}
