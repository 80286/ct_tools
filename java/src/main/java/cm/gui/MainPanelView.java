package main.java.cm.gui;

import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import java.awt.Dimension;

import main.java.cm.Program;

public class MainPanelView extends JPanel {
    private JLabel msgLabel;

    public MainPanelView(MainContext context) {
        MemoryPanelView memoryPanelView;
        SourcePanelView sourcePanelView;
        ControlPanelView controlPanelView;

        memoryPanelView = context.getMemoryPanelController().getView();
        sourcePanelView = context.getSourcePanelController().getView();
        controlPanelView = context.getControlPanelController().getView();

        this.msgLabel = new JLabel("");
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(this.buildTopPanel(memoryPanelView));
        this.add(this.buildBottomPanel(sourcePanelView, controlPanelView));
        this.add(this.msgLabel);
    }

    private JPanel buildTopPanel(JPanel memoryPanel) {
        JPanel topPanel;

        topPanel = new JPanel();
        topPanel.add(memoryPanel);

        return topPanel;
    }

    private JPanel buildBottomPanel(JPanel sourcePanel, JPanel controlPanel) {
        JPanel bottomPanel;

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        bottomPanel.add(sourcePanel);
        bottomPanel.add(controlPanel);

        return bottomPanel;
    }

    public void showJFrame() {
        JFrame mainFrame;

        mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setTitle("CM GUI Test");
        mainFrame.setMinimumSize(new Dimension(640, 480));
        mainFrame.getContentPane().add(this);
        mainFrame.setVisible(true);
    }

    public JLabel getMessageLabel() {
        return this.msgLabel;
    }
}
