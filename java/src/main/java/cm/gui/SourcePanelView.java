package main.java.cm.gui;

import javax.swing.text.DefaultHighlighter;
import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import java.util.List;
import java.util.ArrayList;

import main.java.common.TextLineNumber;

public class SourcePanelView extends JPanel {
    private String STR_TITLE = "Code";

    private List <JScrollPane> textWindows;
    private JTabbedPane tabbedPane;

    public SourcePanelView() {
        this.setBorder(BorderFactory.createTitledBorder(STR_TITLE));
        this.tabbedPane = new JTabbedPane();
        this.textWindows = new ArrayList <JScrollPane> ();

        this.setLayout(new BorderLayout());
        this.add(this.tabbedPane, BorderLayout.CENTER);
    }

    private JScrollPane buildTextArea() {
        JScrollPane scrollPane;
        JTextArea pane;

        pane = new JTextArea();
        scrollPane = new JScrollPane(pane);
        scrollPane.setRowHeaderView(new TextLineNumber(pane));

        return scrollPane;
    }

    public JScrollPane addTab(String tabTitle) {
        JScrollPane newTextWindow;

        newTextWindow = this.buildTextArea();
        this.textWindows.add(newTextWindow);
        this.tabbedPane.addTab(tabTitle, newTextWindow);

        return newTextWindow;
    }

    public JTabbedPane getTabbedPane() {
        return this.tabbedPane;
    }
}
