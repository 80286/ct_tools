package main.java.cm.gui;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import java.util.List;
import java.util.ArrayList;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JFileChooser;
import javax.swing.JComponent;
import javax.swing.JButton;
import javax.swing.JSpinner;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import main.java.common.ListOfStrings;
import main.java.common.LabeledJSpinner;
import main.java.cm.Program;

import java.lang.reflect.Method;

public class ControlPanelController implements ActionListener, ChangeListener {
    private File lastDirectory = null;
    private MainContext mainContext;
    private ControlPanelView view;
    private Thread programThread;
    private int timeInterval = 500;
    
    public ControlPanelController(MainContext context) {
        this.mainContext = context;
        this.view = new ControlPanelView();
        this.connectButtons();
        this.connectSpinners();
    }

    public int getTimeInterval() {
        return this.timeInterval;
    }

    public ControlPanelView getView() {
        return this.view;
    }

    public void connectButtons() {
        List <JButton> buttons;

        buttons = this.view.getButtons();

        for(JButton button: buttons) {
            button.addActionListener(this);
        }
    }

    public void connectSpinners() {
        List <LabeledJSpinner> spinners;

        spinners = this.view.getSpinners();

        for(LabeledJSpinner spinner: spinners) {
            spinner.addChangeListener(this);
        }
    }

    public void updateProgramInformations(String path) {
        MainPanelController mainPanel;
        Program mainProgram;
        String msg;

        mainPanel = this.mainContext.getMainPanelController();
        mainProgram = this.mainContext.getProgram();
        msg = String.format("File='%s'; %d instructions.", 
                path, mainProgram.getNumberOfInstructions());
        mainPanel.setMessage(msg);
    }

    public void tryToParseProgramCode() {
        SourcePanelController sourcePanel;
        MainPanelController mainPanel;
        Program mainProgram;

        mainProgram = this.mainContext.getProgram();
        mainPanel = this.mainContext.getMainPanelController();
        sourcePanel = this.mainContext.getSourcePanelController();

        try {
            mainProgram.doPreRunAction();
            mainProgram.loadListOfStrings(sourcePanel.getListOfStrings());
        } catch(Exception e) {
            mainPanel.setMessage(e.getMessage());
        }
    }

    public void tryToLoadProgramFile(String path) {
        List <String> currentCode;
        MainPanelController mainPanel;
        SourcePanelController sourcePanel;
        Program mainProgram;

        mainProgram = this.mainContext.getProgram();
        mainPanel = this.mainContext.getMainPanelController();
        sourcePanel = this.mainContext.getSourcePanelController();

        try {
            currentCode = ListOfStrings.buildFromFile(path);
            mainProgram.setPath(path);
            sourcePanel.setListOfStrings(currentCode);
            sourcePanel.setTitleOfCurrentTab(mainProgram.getFilename());
            // mainProgram.loadListOfStrings(currentCode);
            // this.updateProgramInformations(path);
        } catch(Exception e) {
            mainPanel.setMessage(e.getMessage());
        }
    }

    public void setChooserDir(JFileChooser chooser) {
        File workingDirectory;

        try {
            if (this.lastDirectory == null) {
                workingDirectory = new File(System.getProperty("user.dir"));
            } else {
                workingDirectory = this.lastDirectory;
            }
            chooser.setCurrentDirectory(workingDirectory);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void actionRun(JComponent source) {
        SourcePanelController sourcePanel;
        MemoryPanelController memoryPanel;
        Program program;
        
        sourcePanel = this.mainContext.getSourcePanelController();
        memoryPanel = this.mainContext.getMemoryPanelController();
        program = this.mainContext.getProgram();
        sourcePanel.disableCurrentTextArea();

        this.tryToParseProgramCode();
        sourcePanel.setListOfStrings(program.getFormattedCode());
        memoryPanel.saveValuesToMemory();

        try {
            program.doPreRunAction();
        } catch(Exception e) {
            e.printStackTrace();
        }

        this.programThread = new Thread() {
            private MainContext context;

            public Thread init(MainContext context) {
                this.context = context;

                return this;
            }

            public void run() {
                SourcePanelController sourcePanel;
                MemoryPanelController memoryPanel;
                ControlPanelController controlPanel;
                Program program;
                int timeInterval;
                boolean isDelayedExecution;

                program = this.context.getProgram();
                controlPanel = this.context.getControlPanelController();
                sourcePanel = this.context.getSourcePanelController();
                memoryPanel = this.context.getMemoryPanelController();
                timeInterval = controlPanel.getTimeInterval();
                isDelayedExecution = (timeInterval != 0);

                while(program.hasNextCommand()) {
                    if (isDelayedExecution) {
                        sourcePanel.removeCurrentTextAreaHighlights();
                        sourcePanel.highlightExecutedLine(program.getCommandCounter());
                    }

                    try {
                        program.executeNextCommand();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }

                    if (isDelayedExecution) {
                        memoryPanel.updateSpinners();
                    }

                    try {
                        Thread.sleep(timeInterval);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
                sourcePanel.removeCurrentTextAreaHighlights();
                sourcePanel.enableCurrentTextArea();
                memoryPanel.updateSpinners();
            }
        }.init(this.mainContext);

        this.programThread.start();
    }

    public void actionStop(JComponent source) {
        SourcePanelController sourcePanel;

        if (this.programThread == null) {
            return;
        }

        this.programThread.interrupt();
        
        sourcePanel = this.mainContext.getSourcePanelController();
        sourcePanel.removeCurrentTextAreaHighlights();
        sourcePanel.enableCurrentTextArea();
    }

    public void actionStep(JComponent source) {
        SourcePanelController sourcePanel;
        Program program;

        program = this.mainContext.getProgram();
        sourcePanel = this.mainContext.getSourcePanelController();

        if(!program.hasNextCommand()) {
            this.actionStop(source);
            return;
        }

        sourcePanel.removeCurrentTextAreaHighlights();
        sourcePanel.highlightExecutedLine(program.getCommandCounter());

        try {
            program.executeNextCommand();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void actionLoad(JComponent source) {
        JFileChooser fileChooser;
        File selectedFile;
        int answer;

        fileChooser = new JFileChooser();
        this.setChooserDir(fileChooser);
        answer = fileChooser.showOpenDialog(this.view);

        if (answer == JFileChooser.APPROVE_OPTION) {
            this.lastDirectory = fileChooser.getCurrentDirectory();
            selectedFile = fileChooser.getSelectedFile();
            this.tryToLoadProgramFile(selectedFile.getPath());
        }
    }

    public void actionSave(JComponent source) {
        MainPanelController mainPanel;
        List <String> currentCode;
        JFileChooser fileChooser;
        File selectedFile;
        int answer;

        mainPanel = this.mainContext.getMainPanelController();
        fileChooser = new JFileChooser();
        this.setChooserDir(fileChooser);
        answer = fileChooser.showSaveDialog(this.view);

        if (answer == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
            currentCode = this.mainContext.getSourcePanelController().getListOfStrings();

            try {
                ListOfStrings.saveToFile(currentCode, selectedFile.getPath());
                mainPanel.setMessage(String.format("Saved to %s.", selectedFile.getName()));
            } catch(IOException e) {
                mainPanel.setMessage(e.getMessage());
            }
        }
    }

    public void actionClearRegisters(JComponent source) {
        MemoryPanelController memoryPanel;
        Program program;

        program = this.mainContext.getProgram();
        memoryPanel = this.mainContext.getMemoryPanelController();

        program.registers.clear();
        memoryPanel.updateSpinners();
        memoryPanel.saveValuesToMemory();
    }

    public void actionClearCode(JComponent source) {
        this.mainContext.getSourcePanelController().clearCurrentPanel();
    }

    public void actionNumOfRegisters(JComponent source) {
        JSpinner sourceSpinner;
        Integer spinnerIntegerValue;
        int chosenNumberOfRegisters;

        sourceSpinner = (JSpinner)source;
        spinnerIntegerValue = (Integer)sourceSpinner.getValue();
        chosenNumberOfRegisters = spinnerIntegerValue.intValue();

        this.mainContext.getMemoryPanelController().resizeTo(chosenNumberOfRegisters);
    }

    public void actionTimeInterval(JComponent source) {
        JSpinner sourceSpinner;
        Integer spinnerIntegerValue;

        sourceSpinner = (JSpinner)source;
        spinnerIntegerValue = (Integer)sourceSpinner.getValue();
        this.timeInterval = spinnerIntegerValue.intValue();
    }

    public void invokeActionMethodByName(String name, JComponent sourceComponent) throws Exception {
        String invokedMethodName;
        Method method;

        invokedMethodName = "action" + name;
        method = this.getClass().getMethod(invokedMethodName, JComponent.class);
        method.invoke(this, sourceComponent);
    }

    public void action(Object sourceObject) {
        String sourceObjectName;
        LabeledJSpinner spinner;
        JButton button;

        if (sourceObject instanceof JButton) {
            button = (JButton)sourceObject;
            sourceObjectName = button.getText();
        } else if(sourceObject  instanceof JSpinner) {
            spinner = (LabeledJSpinner)sourceObject;
            sourceObjectName = spinner.getLabel();
        } else {
            return;
        }

        try {
            this.invokeActionMethodByName(sourceObjectName, (JComponent)sourceObject);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void stateChanged(ChangeEvent event) {
        this.action(event.getSource());
    }

    public void actionPerformed(ActionEvent event) {
        this.action(event.getSource());
    }
}
