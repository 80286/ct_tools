package main.java.cm;

import java.lang.Exception;
import java.lang.Integer;
import java.util.List;

public class Command {
    public final static String MSG_CommandStrNotFound = "Command '%s' not found.";
    private List <Integer> args;
    private String functionName;
    private String comment = null;
    private Function function = null;
    private int dstRegisterIndex = -1;

    public Command(String functionName, List <Integer> args) {
        this.functionName = functionName;
        this.args = args;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void addArgument(Integer arg) {
        this.args.add(arg);
    }

    public void setDstRegisterIndex(int index) {
        this.dstRegisterIndex = index;
    }

    public void linkWithProgram(Program program) throws Exception {
        this.function = program.getFunctionByName(this.functionName);
        if (this.function == null) {
            throw new Exception(
                    String.format(MSG_CommandStrNotFound, this.functionName));
        }
    }

    public void run(Program program) throws Exception {
        this.function.call(program, this.args, this.dstRegisterIndex);
    }

    public List <Integer> getArgs() {
        return this.args;
    }

    public String getFunctionName() {
        return this.functionName;
    }

    public int getDstRegisterIndex() {
        return this.dstRegisterIndex;
    }

    public boolean equals(Command other) {
        return this.functionName.equals(other.getFunctionName())
            && this.args.equals(other.getArgs())
            && (this.dstRegisterIndex == other.getDstRegisterIndex());
    }

    public String getCode() {
        String arguments;

        arguments = this.args.toString().replace("[", "");
        arguments = arguments.replace("]", "");

        if (this.comment == null || this.comment.isEmpty()) {
            return String.format("%s(%s)", 
                    this.functionName, 
                    arguments);
        } else {
            return String.format("%s(%s) %s", 
                    this.functionName, 
                    arguments, 
                    this.comment);
        }
    }

    public void print() {
        System.out.printf("%s(args=%s)\n", this.functionName, this.args.toString());
    }
}
