package main.java.cm.BasicFunctions;

import java.lang.Integer;
import java.util.List;
import main.java.cm.Function;
import main.java.cm.Program;
import main.java.cm.Memory;

public class F_RegisterIncrement extends Function {
    public F_RegisterIncrement() {
        super.setExpectedNumberOfArgs(1);
    }

    public void call(Program program, List <Integer> args, int resultIndex) throws Exception {
        int registerIndexToIncrement = args.get(0);
        int registerValue = program.registers.get(registerIndexToIncrement);

        program.registers.set(registerIndexToIncrement, registerValue + 1);
        program.gotoNextCommand();
    }
}
