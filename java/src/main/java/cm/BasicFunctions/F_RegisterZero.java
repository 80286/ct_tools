package main.java.cm.BasicFunctions;

import java.lang.Integer;
import java.util.List;
import main.java.cm.Function;
import main.java.cm.Program;
import main.java.cm.Memory;

public class F_RegisterZero extends Function {
    public F_RegisterZero() {
        super.setExpectedNumberOfArgs(1);
    }

    public void call(Program program, List <Integer> args, int resultIndex) throws Exception {
        int registerIndexToZero = args.get(0);

        program.registers.set(registerIndexToZero, 0);
        program.gotoNextCommand();
    }
}
