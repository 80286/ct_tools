package main.java.cm.BasicFunctions;

import java.lang.Integer;
import java.util.List;
import main.java.cm.Function;
import main.java.cm.Program;
import main.java.cm.Memory;

public class F_InstructionJump extends Function {
    public F_InstructionJump() {
        super.setExpectedNumberOfArgs(3);
    }

    public void call(Program program, List <Integer> args, int resultIndex) throws Exception {
        int regIndex1 = args.get(0).intValue();
        int regIndex2 = args.get(1).intValue();
        int dstCommandIndex = args.get(2).intValue();

        int reg1Value = program.registers.get(regIndex1);
        int reg2Value = program.registers.get(regIndex2);

        if (reg1Value == reg2Value) {
            program.gotoToCommand(dstCommandIndex);
        } else {
            program.gotoNextCommand();
        }
    }
}

