package main.java.cm.BasicFunctions;

import java.lang.Integer;
import java.util.List;
import main.java.cm.Function;
import main.java.cm.Program;
import main.java.cm.Memory;

public class F_RegisterCopy extends Function {
    public F_RegisterCopy() {
        super.setExpectedNumberOfArgs(2);
    }

    public void call(Program program, List <Integer> args, int resultIndex) throws Exception {
        int srcRegIndex = args.get(0);
        int dstRegIndex = args.get(1);
        int srcValue = program.registers.get(srcRegIndex);

        program.registers.set(dstRegIndex, srcValue);
        program.gotoNextCommand();
    }
}
