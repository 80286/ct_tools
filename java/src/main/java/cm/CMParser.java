package main.java.cm;

import java.util.Map;
import java.util.List;

interface CMParser {
    public Map <String, String> parseComment(String comment) throws Exception;
    // public List <Object> parseCommand(String cmd) throws Exception;
    public Command parseCommand(String cmd) throws Exception;
    public String [] getCommandAndComment(String line);
}
