package main.java.cm;

import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class ProgramParser implements CMParser {
    public static final String MSG_ExpectedFunctionName = "Expected function name.";
    public static final String MSG_ExpectedOpenningBracket = "Expected openning bracket.";
    public static final String MSG_ExpectedClosingBracket = "Expected closing bracket.";
    public static final String MSG_MismatchedBrackets = "Mismatched brackets.";
    public static final String MSG_ExpectedStrFoundStr = "Expected '%s', found '%s'.";
    public static final String MSG_ExpectedResultRegisterFoundStr = "Expected result register, found '%s'.";

    public static final String CMT_WrongSettingAtStr = "Wrong setting syntax at '%s'.";
    public static final String CMT_ExpectedImportFoundStr = "Expected 'import', found '%s'.";

    public static final String COMMENT_PREFIX = "cm:";
    public static final char COMMENT_SYM = '#';
    /**
     * Converts comment to hash map.
     *
     * @param comment
     *      String containing line comment e.g. "cm: title=abs argc=1".
     * @throws Exception
     *      If comment has bad syntax.
     * @return
     *      null, if comment has no proper prefix, otherwise hash map with
     *      set of options related with comment.
     *
     * Examples of correct comments:
     * "cm: title=Abs"
     * "cm: title=Abs argc=1"
     * "cm: title=Abs argc=1 import:zero.inc"
     */
    public Map <String, String> parseComment(String comment) throws Exception {
        Map <String, String> result = null;
        int pos;

        if (comment.isEmpty() || comment.charAt(0) != COMMENT_SYM) {
            return result;
        }

        comment = comment.substring(1).trim();
        String [] args = comment.split(" ");

        if (args.length == 0 || !args[0].equals(COMMENT_PREFIX)) {
            return result;
        }

        result = new HashMap <String, String> ();

        for(int i = 1; i < args.length; i++) {
            String setting = args[i];
            String wrongSyntaxMsg = String.format(CMT_WrongSettingAtStr, setting);
            String unexpectedMsg = String.format("Unexpected '%s'.", setting);

            if(setting.indexOf("=") != -1) {
                /* "key=value" */
                String [] parts = setting.split("=");

                if (parts.length != 2) {
                    throw new Exception(wrongSyntaxMsg);
                }

                String key = parts[0];
                String value = parts[1];

                result.put(key, value);
            } else if(setting.indexOf(":") != -1) {
                String [] parts = setting.split(":");

                if (parts.length != 2) {
                    throw new Exception(wrongSyntaxMsg);
                }

                if (!parts[0].equals("import")) {
                    throw new Exception(
                            String.format(CMT_ExpectedImportFoundStr, parts[0]));
                }

                String importedFiles = result.get(parts[0]);
                String newImportList;

                if (importedFiles == null || importedFiles.isEmpty()) {
                    newImportList = parts[1];
                } else {
                    newImportList = String.format("%s,%s", importedFiles, parts[1]);
                }

                result.put(parts[0], newImportList);
            } else {
                throw new Exception(unexpectedMsg);
            }
        }

        return result;
    }
    
    /**
     * Split string containing call of function to list of strings.
     *
     * @param cmd
     *      String with command.
     * @throws Exception
     *      If command has wrong syntax.
     * @return
     *      List of objects with function name (one String object) 
     *      and attached arguments (Integers object(s)).
     */
    public Command parseCommand(String cmd) throws Exception {
        String functionArgs;
        String functionName;
        String extraArgs;
        int leftBracketIndex;
        int rightBracketIndex;

        leftBracketIndex = cmd.indexOf('(');

        if (leftBracketIndex == 0) {
            throw new Exception(MSG_ExpectedFunctionName);
        }

        if (leftBracketIndex == -1) {
            throw new Exception(MSG_ExpectedOpenningBracket);
        }

        rightBracketIndex = cmd.indexOf(')');

        if (rightBracketIndex == -1) {
            throw new Exception(MSG_ExpectedClosingBracket);
        } else if(rightBracketIndex < leftBracketIndex) {
            throw new Exception(MSG_MismatchedBrackets);
        }

        functionName = cmd.substring(0, leftBracketIndex);

        Command result = new Command(functionName, new ArrayList <Integer> ());

        functionArgs = cmd.substring(leftBracketIndex + 1, rightBracketIndex);
        extraArgs = cmd.substring(rightBracketIndex + 1, cmd.length()).trim();

        String [] argsArray = functionArgs.trim().split(" "); 

        if (functionArgs.isEmpty()) {
            return result;
        }

        if (!extraArgs.isEmpty()) {
            Integer subfunctionDstRegisterIndex;

            String [] extraArgsArray = extraArgs.split(" ");

            if (extraArgsArray.length > 0) {
                if (extraArgsArray[0].equals("->")) {
                    if (extraArgsArray.length == 2) {
                        subfunctionDstRegisterIndex = new Integer(extraArgsArray[1]);
                        result.setDstRegisterIndex(subfunctionDstRegisterIndex);
                    } else {
                        throw new Exception(
                                String.format(MSG_ExpectedResultRegisterFoundStr, 
                                    extraArgsArray[0]));
                    }
                } else {
                    // Unexpected '%s'.
                    throw new Exception(
                            String.format(MSG_ExpectedResultRegisterFoundStr,
                                    extraArgs));
                }
            }
        }

        int lastArgIndex = argsArray.length - 1;

        for(int i = 0; i < argsArray.length; i++) {
            String arg = argsArray[i];
            char lastChar = arg.charAt(arg.length() - 1);
            Integer intArg = null;

            if (i != lastArgIndex) {
                if(lastChar != ',') {
                    throw new Exception(
                            String.format(MSG_ExpectedStrFoundStr, ",", arg));
                }

                /* Remove coma: */
                arg = arg.substring(0, arg.length() - 1);
            }

            try {
                intArg = new Integer(arg);
            } catch(NumberFormatException e){
                throw new Exception(e.toString());
            }

            result.addArgument(intArg);
        }

        return result;
    }

    /**
     * Splits program line into 2 strings - execution of command and
     * comment line.
     *
     * @param line
     *      String with line of program.
     * @return
     *      Array of 2 strings, first with command and second with content
     *      of line comment.
     */
    public String [] getCommandAndComment(String line) {
        String [] result = new String[2];
        int commentSymbolIndex;

        if (line.isEmpty()) {
            result[0] = "";
            result[1] = "";
            return result;
        }

        commentSymbolIndex = line.indexOf(COMMENT_SYM);

        if (commentSymbolIndex == -1) {
            result[0] = line.trim();
            result[1] = "";
        } else {
            if (commentSymbolIndex == 0) {
                result[0] = "";
                result[1] = line.trim();
            } else {
                result[0] = line.substring(0, commentSymbolIndex).trim();
                result[1] = line.substring(commentSymbolIndex, line.length()).trim();
            }
        }

        return result;
    }
}
