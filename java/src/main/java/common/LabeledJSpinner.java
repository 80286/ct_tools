package main.java.common;

import javax.swing.JSpinner;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class LabeledJSpinner extends JSpinner {
    private String label;

    public LabeledJSpinner(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public JPanel toJPanel() {
        JPanel panel = new JPanel();

        panel.add(new JLabel(this.label));
        panel.add(this);

        return panel;
    }
}
