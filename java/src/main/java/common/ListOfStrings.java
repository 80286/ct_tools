package main.java.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

public class ListOfStrings extends ArrayList <String> {
    public static List <String> buildFromFile(String path) throws IOException {
        BufferedReader bufferedReader;
        List <String> lines;
        FileReader reader;
        String line;

        lines = new ArrayList <String> ();
        reader = new FileReader(path);
        bufferedReader = new BufferedReader(reader);

        while((line = bufferedReader.readLine()) != null) {
            lines.add(line.trim());
        }

        bufferedReader.close();

        return lines;
    }

    public static void saveToFile(List <String> lines, String path) throws IOException {
        BufferedWriter writer;
        String newLineCharacter;

        newLineCharacter = System.lineSeparator();
        writer = new BufferedWriter(new FileWriter(path));

        for(String string: lines) {
            writer.write(string + newLineCharacter);
        }

        writer.close();
    }
}
