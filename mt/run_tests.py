#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from src.MachineProgramExecutor import *
from src.ExecutionHandler import *


def main():
    handler = ExecutionHandler()
    acceptState = "qa"
    rejectState = "qr"

    tests = [
        ("mt_length.in", [
            ("000|111",             True,   None),
            ("00000000|11111111",   True,   None),
            ("0000|11",             False,  None)
        ]),
        ("mt_regex1.in", [
            ("",                    True,   None),
            ("ab",                  True,   None),
            ("abab",                True,   None),
            ("ababa",               False,  None)
        ]),
        ("mt_pdrom.in", [
            ("010",                 True,   None),
            ("01011",               False,  None),
            ("1101011",             True,   None),
            ("1",                   True,   None),
        ]),
        ("mt_bin_inc.in", [
            ("1",                   "10",       -1),
            ("0",                   "1",        -1),
            ("100",                 "101",      -1),
            ("111111",              "1000000",  -1),
        ]),
        ("mt_2x_y.in", [
            ("1111|11",             "111111",   0),
            ("11|1",                "111",   0),
        ]),
    ]

    for testProgram, testCases in tests:
        for testWord, expectedResult, resultTapeIdx in testCases:
            argv = ["", testProgram, testWord]
            ctx = MachineProgramExecutor.main(argv, handler)
            finalStateId = ctx.stateId

            msg = ""
            found = ""

            if expectedResult in (False, True):
                shouldAccept = expectedResult
                isSuccess = (finalStateId == acceptState)
                isFail = (finalStateId == rejectState)

                if isSuccess == shouldAccept:
                    msg = "OK"
                    found = isSuccess
                elif isFail == shouldAccept:
                    msg = "OK"
                    found = isFail
                else:
                    msg = "FAILED"
            else:
                lastTapeResult = ctx.tapes[resultTapeIdx].getStrippedData()
                found = lastTapeResult

                msg = "OK" if expectedResult == lastTapeResult else "FAILED"

            print(f"{msg}: {testProgram}; input: '{testWord}'; expected: {expectedResult}; found: {found}")


if __name__ == "__main__":
    main()
