## Symulator Wielotaśmowej Maszyny Turinga (CLI)

![Screenshot](https://bitbucket.org/80286/ct_tools/raw/master/mt/120116_mt.png)

### Uruchomienie symulatora:

Uruchomienie programu `mt_length.in` dla słowa wejściowego `000|111`:

```
$ ./mt.py mt_length.in '000|111'
```

Program wyświetli animację wykonania każdego etapu ustalonego w wskazanym programie po czym zatrzyma się jeśli osiągnie
stan akceptujący lub odrzucający.

Wskazany przykład przygotowany dla 3 taśmowej MT sprawdzi czy podane słowo wejściowe składa się z dwóch napisów o równej długości.

Nie należy używać `#` jako symbolu na taśmie w programach zarezerwowany dla komentarza liniowego.
Uzyć w zamian symboli `|` lub `!`.

### Format instrukcji w pliku programu

Dla programu korzystającego z 3 taśm:

```
q0 0 _ _ q1 0 0 _ > > S
```

Dla 1 taśmy:

```
q0 0 q1 0 >
```

Lista możliwych operacji na pojedynczej taśmie:

- `S` : pozostaw głowicę w niezmienionej pozycji
- `<` : przesuń głowicę w lewą stronę
- `>` : przesuń głowicę w prawą stronę

Ogólna składnia dla instrukcji w maszynie `n`-taśmowej to:

```
<nazwa_stanu_opisywanego> <a_1> <a_2> ... <a_n> <nazwa_stanu_docelowego> <b_1> <b_2> ... <b_n> <c_1> <c_2> ... <c_n>
```

Gdzie:

- `<a_1> ... <a_n>` - lista oczekiwanych symboli na każdej z `n` taśm.
- `<b_1> ... <b_n>` - lista symboli do wpisania na aktualnej pozycji w każdej z taśm (w razie dopasowania do oczekiwanych symboli)
- `<c_1> ... <c_n>` - lista operacji na każdej z taśm wykonywana w razie dopasowania (`S / < / >`)

### Dodatkowe informacje które mogą zostać ustalone w plikach z programami

Dyrektywy w pliku wejściowym zaczynają się w komentarzu liniowym po prefiksie `mt:`

Ilość taśm w maszynie:

```
mt: tapes=1
```

Nazwa stanu akceptującego:

```
mt: accept=qAccept
```

Nazwa stanu odrzucającego:

```
mt: reject=qReject
```

Nazwa stanu początkowego:

```
mt: start=qStart
```

Odstęp między każdym wyczyszczeniem ekranu terminala.

```
mt: rtime=0.25
```

Tytuł programu, przy czym znaki `_` zamienione zostaną na spacje.

```
mt: title=Test_parzystosci_liczby
```
