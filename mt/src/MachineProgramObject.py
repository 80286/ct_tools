

class MachineProgramObject(object):
    transitionSymbolRight = ">"
    transitionSymbolLeft = "<"
    transitionSymbolStay = "S"
    transitionSymbols = (transitionSymbolLeft, transitionSymbolRight, transitionSymbolStay)

    def __init__(self):
        self._lineCommentSymbol = "#"
        self.states = {}
        self.numOfTapes = 1
        self.format1_argc = 5
        self.programTitle = None
        self.refreshTime = 0.25

        self.startState  = "q0"
        self.acceptState = "qa"
        self.rejectState = "qr"
        self.program = ""

    def _parseLineComment(self, nl, line):
        args = line.split()
        argc = len(args)

        if argc <= 1:
            return

        if args[1] != "mt:":
            return

        if argc == 1:
            print("{0}:{1}: Expected program settings.".format(self.program, nl))
            return

        args = args[2:]

        for arg in args:
            if arg.find("=") != -1:
                setting = arg.split("=")

                if len(setting) != 2:
                    print("{0}:{1}: Syntax error at '{2}.".format(self.program, nl, line))
                    return

                key = setting[0]
                val = setting[1]

                self._setProgramVariable(nl, key, val)
            else:
                print("{0}:{1}: Unexpected '{2}'.".format(self.program, nl, arg))

    def _setProgramVariable(self, nl, key, val):
        if key == "tapes":
            self.numOfTapes = int(val)
            self.format1_argc = 2 + 3 * self.numOfTapes
        elif key == "rtime":
            nval = float(val)
            if nval <= 0.0:
                return

            self.refreshTime = float(val)
        elif key == "start":
            self.startState = val
        elif key == "accept":
            self.acceptState = val
        elif key == "reject":
            self.rejectState = val
        elif key == "title":
            self.programTitle = val.replace("_", " ")
        else:
            print("{0}:{1}: Unknown parameter '{2}'".format(self.program, nl, key))

    def parseLineFormat1(self, nl, line):
        numOfTapes = self.numOfTapes

        # Format1: 'q0 a b c q1 a b c' >>>
        args = line.split()

        if len(args) != self.format1_argc:
            print("{0}:{1}: Expected {2} args ({3} tapes).".format(self.program, nl, self.format1_argc, numOfTapes))
            return

        srcState = args[0]
        dstState = args[1 + numOfTapes]

        srcValues = ""
        dstValues = ""
        tapeMove = ""

        for i in range(numOfTapes):
            srcSym = args[i + 1]
            dstSym = args[numOfTapes + 2 + i]
            mov = args[2 + 2 * numOfTapes + i]

            if len(srcSym) != 1:
                print("{0}:{1}: Too long tape symbol found.".format(self.program, nl, srcSym))
                return

            if len(dstSym) != 1:
                print("{0}:{1}: Too long tape symbol found.".format(self.program, nl, dstSym))
                return

            if len(mov) != 1 or (mov not in self.transitionSymbols):
                print("{0}:{1}: Unknown movement command '{2}'.".format(self.program, nl, mov))
                return

            srcValues += srcSym
            dstValues += dstSym
            tapeMove += mov

        if srcState not in self.states.keys():
            self.states[srcState] = []

        self.states[srcState].append((srcValues, dstState, dstValues, tapeMove))

    def printSettings(self):
        print("Number of tapes: {0}".format(self.numOfTapes))
        print("Refresh time:    {0}".format(self.refreshTime))
        print("Start:           {0}".format(self.startState))
        print("Accept:          {0}".format(self.acceptState))
        print("Reject:          {0}".format(self.rejectState))

    def load(self, filename):
        self.program = filename
        lines = None

        with open(filename, "r") as f:
            lines = f.readlines()
        
        lineNum = 0

        for line in lines:
            lineNum += 1
            hpos = line.find(self._lineCommentSymbol)
            lineComment = ""

            if hpos != -1:
                lineComment = line[hpos:]
                self._parseLineComment(lineNum, lineComment)
                line = line[:hpos]

            line = line.strip()

            if line == "" or len(line) == 0:
                continue

            self.parseLineFormat1(lineNum, line)
