from .Colors import *


class MachineTape(object):
    emptySymbol = "_"

    def __init__(self):
        self._dataLen = 100
        self._dataIdx = int(self._dataLen / 2)
        self._data = [self.emptySymbol for _ in range(self._dataLen)]
        self._numOfVisibleCells = 18

    def moveLeft(self):
        if self._dataIdx != 0:
            self._dataIdx -= 1

    def moveRight(self):
        if self._dataIdx != (self._dataLen - 1):
            self._dataIdx += 1

    def put(self, word):
        for i, sym in enumerate(word):
            self._data[self._dataIdx + i] = sym

    def set(self, value):
        self._data[self._dataIdx] = str(value)

    def get(self):
        return self._data[self._dataIdx]

    def getStrippedData(self):
        return "".join(self._data).strip(self.emptySymbol)

    def _printSeparator(self, c="="):
        s = 4 * c

        for _ in range(self._numOfVisibleCells):
            print(s, end="")

        print("")

    def _getLeftEdgeIdx(self):
        return self._dataIdx - int(self._numOfVisibleCells / 2)

    def _isValidIdx(self, idx):
        return (idx >= 0) or (idx < self._dataLen)

    def _print(self):
        sep = ("|", "!", "$")
        idx = self._getLeftEdgeIdx()

        for _ in range(self._numOfVisibleCells):
            if not self._isValidIdx(idx):
                print("   |", end="")
                continue

            cell = self._data[idx]

            if cell == self.emptySymbol:
                cellColor = Colors.blue
            elif cell in sep:
                cellColor = Colors.red
            else:
                cellColor = Colors.green2
            
            cell = Colors.colorize(cell, cellColor)

            print(" {0} |".format(cell), end="")
            idx += 1

        print("")

    def _displayIndices(self):
        idx = self._getLeftEdgeIdx()

        for _ in range(self._numOfVisibleCells):
            print(" %2d " %(idx), end="")
            idx += 1

        print("")


    def _printHeader(self):
        idx = self._getLeftEdgeIdx()
        hs = Colors.colorize(" ^  ", Colors.green)

        for _ in range(self._numOfVisibleCells):
            print("    " if self._dataIdx != idx else hs, end="")
            idx += 1

        print("")

    def display(self):
        # self._displayIndices()
        self._printSeparator("_")
        self._print()
        self._printSeparator("_")
        self._printHeader()
