class Colors(object):
    blue = (1, 34, 40)
    green = (0, 32, 40)
    green2 = (1, 37, 40)
    red = (1, 31, 40)

    @staticmethod
    def colorize(s, col):
        return '\033[' + str(col[0]) + ';' + str(col[1]) + ";" + str(col[2]) + "m" + s + "\033[0m"
