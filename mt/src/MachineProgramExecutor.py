from .MachineProgramObject import *
from .DefaultExecutionHandler import *
from .ExecutorContext import *


class MachineProgramExecutor(object):
    def run(self, programObj, inputWord, handler):
        numOfTapes = programObj.numOfTapes
        states = programObj.states

        ctx = ExecutorContext(programObj)

        ctx.setNumOfTapes(numOfTapes)
        ctx.setInputWord(inputWord)

        handler.onStart(ctx)

        while ctx.isRunningStateId():
            if not ctx.isValidState():
                print("State '{0}' not found. Terminating.".format(ctx.stateId))
                break

            ctx.instructionIdx += 1

            stateDecisions = states[ctx.stateId]
            foundDecision = True

            for decision in stateDecisions:
                srcSymId = decision[0]
                dstStateId = decision[1]
                dstSym = decision[2]
                transitionIds = decision[3]

                foundDecision = True

                for i in range(numOfTapes):
                    tape = ctx.tapes[i]

                    if srcSymId[i] == tape.get():
                        continue
                    else:
                        foundDecision = False
                        break

                if foundDecision:
                    for i in range(numOfTapes):
                        transitionId = transitionIds[i]

                        tape = ctx.tapes[i]
                        tape.set(dstSym[i])

                        handler.onHeaderMove(ctx)

                        if transitionId == MachineProgramObject.transitionSymbolRight:
                            tape.moveRight()
                        elif transitionId == MachineProgramObject.transitionSymbolLeft:
                            tape.moveLeft()
                        elif transitionId == MachineProgramObject.transitionSymbolStay:
                            pass

                    ctx.changeState(dstStateId)
                    handler.onStateChanged(ctx)
                    break
                else:
                    continue

            if not foundDecision:
                handler.onUnknownDecision(ctx)
                break

        return ctx

    @staticmethod
    def main(argv, handler):
        argc = len(argv)

        if argc < 2:
            print("Usage: {0} prog1.in 'inputWord'".format(argv[0]))
            return

        program = argv[1]
        inputWord = argv[2] if argc == 3 else ""

        try:
            programObj = MachineProgramObject()
            programObj.load(program)

            programExecutor = MachineProgramExecutor()
            return programExecutor.run(programObj, inputWord, handler)
        except KeyboardInterrupt:
            pass
