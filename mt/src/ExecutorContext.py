from .MachineTape import *

class ExecutorContext(object):
    def __init__(self, programObj):
        self.reset(programObj)

    def reset(self, programObj):
        self.instructionIdx = 0
        self.stateId = programObj.startState
        self.programObj = programObj
        self.inputWord = ""
        self.tapes = []

    def setInputWord(self, word):
        self.inputWord = word
        self.tapes[0].put(self.inputWord)

    def setNumOfTapes(self, num):
        if num < 1 or num > 10:
            return

        self.tapes = [MachineTape() for _ in range(num)]
        self.setInputWord(self.inputWord)

    def isRunningStateId(self):
        programObj = self.programObj
        stateId = self.stateId

        return (stateId != programObj.acceptState) and (stateId != programObj.rejectState)

    def isValidState(self):
        return self.stateId in self.programObj.states.keys()

    def changeState(self, newStateId):
        self.stateId = newStateId
