from .ExecutionHandler import *
from .MachineTape import *
from .Colors import *

import time
import os


class DefaultExecutionHandler(ExecutionHandler):
    @staticmethod
    def _printTapes(ctx):
        for tape in ctx.tapes:
            tape.display()

    def _displayFrame(self, ctx):
        stateId = ctx.stateId
        programObj = ctx.programObj
        colorize = Colors.colorize

        os.system('cls' if os.name == 'nt' else 'clear')
        title = programObj.programTitle

        if title:
            print(colorize(" # {0}".format(title), Colors.blue))
            print("-" * (5 + len(title)))

        print("[{0}] Current state: {1}".format(ctx.instructionIdx, colorize(stateId, Colors.green2)))
        self._printTapes(ctx)

        time.sleep(programObj.refreshTime)
        stateMsg = ""

        if stateId == programObj.acceptState:
            stateMsg = colorize("accepted", Colors.green)
        elif stateId == programObj.rejectState:
            stateMsg = colorize("rejected", Colors.red)
        else:
            return

        print("Word '{0}' {1}!".format(ctx.inputWord, stateMsg))

    def onStart(self, ctx):
        self._displayFrame(ctx)

    def onHeaderMove(self, ctx):
        self._displayFrame(ctx)

    def onStateChanged(self, ctx):
        self._displayFrame(ctx)

    def onUnknownDecision(self, ctx):
        tapesContent = "".join([tape.get() for tape in ctx.tapes])
        nullTape = "".join([MachineTape.emptySymbol for _ in range(len(ctx.tapes))])

        print("TapesContent: ", tapesContent)
        print("Decision not found for state '{0}' and tapes content '{1}'.".format(ctx.stateId, tapesContent))

        if tapesContent == nullTape:
            print("Did you forget to load an input word?")
