#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# mt.py
# Copyright (C) 1972 Juzef
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

version = "211214a"

import time
import sys
import os

c_blu = (1, 34, 40)
c_grn = (1, 32, 40)
c_red = (1, 31, 40)
c_gre = (0, 37, 0)
c_gr2 = (1, 37, 40)

def pcol(s, col):
    return '\033[' + str(col[0]) + ';' + str(col[1]) + ";" + str(col[2]) + "m" + s + "\033[0m"

class MT_Tape(object):
    # Constant length of single tape
    TAPE_LENGTH = 100

    # Amount of visible cells in tape:
    VIS_CELLS_NUM = 18

    # Content of tape:
    data = None

    # Header's position:
    header = 0

    def __init__(self):
        self.data = ['_' for i in range(self.TAPE_LENGTH)]
        self.header = int(self.TAPE_LENGTH / 2)

    def move_left(self):
        if self.header != 0:
            self.header -= 1

    def move_right(self):
        if self.header != (self.TAPE_LENGTH - 1):
            self.header += 1

    def put(self, word):
        i = 0
        for sym in word:
            self.data[self.header + i] = sym
            i += 1

    def set(self, value):
        self.data[self.header] = str(value)

    def get(self):
        return self.data[self.header]

    def display_rule(self, c="="):
        s = 4 * c
        for i in range(self.VIS_CELLS_NUM):
            print(s, end="")
        print("")

    def display_tape(self):
        # 50 - 7 = 43
        # [43, 57]
        sep = ("|", "!", "$")
        pos = self.header - int(self.VIS_CELLS_NUM / 2)

        for i in range(self.VIS_CELLS_NUM):
            if pos < 0 or pos > self.TAPE_LENGTH:
                print("   |", end="")
                continue

            cell = self.data[pos]
            if cell == "_":
                cell = pcol(cell, c_blu)
            if cell in sep:
                cell = pcol(cell, c_red)
            else:
                cell = pcol(cell, c_gr2)

            print(" {0} |".format(cell), end="")
            pos += 1
        print("")


    def display_indexes(self):
        pos = self.header - int(self.VIS_CELLS_NUM / 2)
        for i in range(self.VIS_CELLS_NUM):
            print(" %2d " %(pos), end="")
            pos += 1
        print("")


    def display_header(self):
        pos = self.header - int(self.VIS_CELLS_NUM / 2)
        hs = pcol(" ^  ", c_grn)
        for i in range(self.VIS_CELLS_NUM):
            print("    " if self.header != pos else hs, end="")
            pos += 1
        print("")

    def display(self):
        # self.display_indexes()
        self.display_rule("_")
        self.display_tape()
        self.display_rule("_")
        self.display_header()


class MT(object):
    # Number of tapes:
    tapes_num = 1

    # List of tapes:
    tapes = None

    LINE_COMMENT_SYM = "#"

    # Right, Left, Stay
    move_symbols = (">", "<", "S")

    program = ""

    program_title = None

    format1_argc = 5

    refresh_time = 0.25

    # Default state names:
    start_state  = "q0"
    accept_state = "qa"
    reject_state = "qr"

    input_word = ""

    # self.states['q0'] = [
    #   ('abc', 'q1', 'abc', '>>>'),
    #   ...
    # ]
    states = {}

    def put_input_word(self, word):
        self.input_word = word
        self.tapes[0].put(self.input_word)

    def _init_tapes(self):
        self.tapes = []
        for i in range(self.tapes_num):
            self.tapes.append(MT_Tape())

    def set_tapes_num(self, num):
        if num < 1 or num > 10:
            return
        self.tapes_num = num
        self.format1_argc = 2 + 3 * self.tapes_num
        self._init_tapes()
        self.put_input_word(self.input_word)

    def print_tapes(self):
        for tape in self.tapes:
            tape.display()

    def print_settings(self):
        print("Number of tapes: {0}".format(self.tapes_num))
        print("Refresh time:    {0}".format(self.refresh_time))
        print("Start:           {0}".format(self.start_state))
        print("Accept:          {0}".format(self.accept_state))
        print("Reject:          {0}".format(self.reject_state))

    def set_program_var(self, nl, key, val):# {{{
        if key == "tapes":
            self.set_tapes_num(int(val))
        elif key == "rtime":
            nval = float(val)
            if nval <= 0.0:
                return

            self.refresh_time = float(val)
        elif key == "start":
            self.start_state = val
        elif key == "accept":
            self.accept_state = val
        elif key == "reject":
            self.reject_state = val
        elif key == "title":
            self.program_title = val.replace("_", " ")
        else:
            print("{0}:{1}: Unknown parameter '{2}'".format(self.program, nl, key))# }}}

    def parse_line_comment(self, nl, line):# {{{
        args = line.split()
        argc = len(args)
        # print(args)

        if argc <= 1:
            return

        if args[1] != "mt:":
            return

        if argc == 1:
            print("{0}:{1}: Expected program settings.".format(self.program, nl))
            return

        args = args[2:]

        for arg in args:
            if arg.find("=") != -1:
                setting = arg.split("=")
                if len(setting) != 2:
                    print("{0}:{1}: Syntax error at '{2}.".format(self.program, nl, line))
                    return
                key = setting[0]
                val = setting[1]
                print("Found key: '{0}', val: '{1}'.".format(key, val))
                self.set_program_var(nl, key, val)
            else:
                print("{0}:{1}: Unexpected '{2}'.".format(self.program, nl, arg))# }}}

    def parse_line_format1(self, nl, line):
        # Format1: 'q0 a b c q1 a b c' >>>
        args = line.split()
        argc = len(args)

        # print("{0}: parse_line_format1({1})".format(nl, line))

        if len(args) != self.format1_argc:
            print("{0}:{1}: Expected {2} args ({3} tapes).".format(
                self.program, nl, self.format1_argc, self.tapes_num))
            return

        src_state = args[0]
        dst_state = args[1 + self.tapes_num]

        src_values = ""
        dst_values = ""
        tap_mov = ""

        for i in range(self.tapes_num):
            src_sym = args[i + 1]
            dst_sym = args[self.tapes_num + 2 + i]
            mov = args[2 + 2 * self.tapes_num + i]

            if len(src_sym) != 1:
                print("{0}:{1}: Too long tape symbol found.".format(self.program, nl, src_sym))
                return

            if len(dst_sym) != 1:
                print("{0}:{1}: Too long tape symbol found.".format(self.program, nl, dst_sym))
                return

            if len(mov) != 1 or (mov not in self.move_symbols):
                print("{0}:{1}: Unknown movement command '{2}'.".format(self.program, nl, mov))
                return

            src_values += src_sym
            dst_values += dst_sym
            tap_mov += mov

        # print("src: ", src_values)
        # print("dst: ", dst_values)
        # print("mov: ", tap_mov)

        if src_state not in self.states.keys():
            self.states[src_state] = []

        self.states[src_state].append((src_values, dst_state, dst_values, tap_mov))

    def load_code(self, filename):
        self.program = filename
        f = None

        try:
            f = open(filename, "r")
        except FileNotFoundError as e:
            print("File '{0}' not found.".format(filename))
            return

        lines = f.readlines()
        line_nr = 0

        for line in lines:
            line_nr += 1
            hpos = line.find(self.LINE_COMMENT_SYM)
            line_comment = ""

            if hpos != -1:
                line_comment = line[hpos:]
                self.parse_line_comment(line_nr, line_comment)
                line = line[:hpos]
                # print("Line comment: '{0}'".format(line_comment.rstrip()))
            # print("Line: '{0}'".format(line.rstrip()))

            line = line.strip()

            if line == "" or len(line) == 0:
                continue

            self.parse_line_format1(line_nr, line)

        f.close()
        return 0

    def display_frame(self, instr_num, state, delay=None):
        os.system('cls' if os.name == 'nt' else 'clear')

        if not self.program_title is None:
            print(pcol(" # {0}".format(self.program_title), c_blu))
            print("-" * (5 + len(self.program_title)))
        print("[{0}] Current state: {1}".format(instr_num, pcol(state, c_gr2)))
        self.print_tapes()

        time.sleep(delay if delay != None else self.refresh_time)

        state_msg = ""

        if state == self.accept_state:
            state_msg = pcol("accepted", c_grn)
        elif state == self.reject_state:
            state_msg = pcol("rejected", c_red)
        else:
            return

        print("Word '{0}' {1}!".format(self.input_word, state_msg))

    def run(self):
        terminate = False
        state = self.start_state
        instr = 0

        self.display_frame(instr, state)

        while (state != self.accept_state) and (state != self.reject_state):
            if state not in self.states.keys():
                print("State '{0}' not found. Terminating.".format(state))
                break

            instr += 1

            # Get list of decisions for current state:
            state_decisions = self.states[state]

            found_decision = True

            for decision in state_decisions:
                src_sym = decision[0]
                dst_st  = decision[1]
                dst_sym = decision[2]
                mov     = decision[3]

                found_decision = True
                # decision format:  ("0__, "q1", "00_", ">>>")

                # print("Decision: ", decision)
                # print("src: ", src_sym)
                # print("dst: ", dst_sym)
                # print("mov: ", mov)

                for i in range(self.tapes_num):
                    # print("Dec: {0} == {1} ?".format(src_sym[i], self.tapes[i].get()))
                    # Checking input symbols on each tape:
                    if src_sym[i] == self.tapes[i].get():
                        continue
                    else:
                        found_decision = False
                        break


                if found_decision:
                    # print("Decision found.")
                    # Perform move for each tape:
                    for i in range(self.tapes_num):
                        m = mov[i]

                        self.tapes[i].set(dst_sym[i])

                        # self.display_frame(instr, state, 0.2)
                        self.display_frame(instr, state)

                        if m == self.move_symbols[0]:
                            # Right
                            self.tapes[i].move_right()
                        elif m == self.move_symbols[1]:
                            # Left
                            self.tapes[i].move_left()
                        elif m == self.move_symbols[2]:
                            # Stay
                            pass
                        else:
                            # ???
                            pass

                    # print("State change: {0} -> {1}".format(state, dst_st))
                    state = dst_st
                    self.display_frame(instr, state)
                    break
                else:
                    continue
                # end for

            if not found_decision:
                tapes_content = "".join([t.get() for t in self.tapes])
                null_content  = "".join(["_" for i in range(self.tapes_num)])

                print("Tapes_content: ", tapes_content)
                # print(null_content)

                print("Decision not found for state '{0}' and tapes content '{1}'.".format(
                    state, tapes_content))

                if tapes_content == null_content:
                    print("Did you forget to load an input word?")
                break

    def __init__(self, tapes_num=1):
        self.tapes_num = tapes_num
        self._init_tapes()

def main():
    mt = MT()

    if len(sys.argv) < 2:
        print("Usage: {0} prog1.in 'input_word'".format(sys.argv[0]))
        return

    infile = sys.argv[1]
    input_word = ""

    if len(sys.argv) == 3:
        input_word = sys.argv[2]

    if mt.load_code(infile) == 0:
        # mt.print_settings()
        mt.put_input_word(input_word)
        try:
            mt.run()
        except KeyboardInterrupt as e:
            pass

if __name__ == '__main__':
    main()
