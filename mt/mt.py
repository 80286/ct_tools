#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from src.MachineProgramExecutor import *
from src.DefaultExecutionHandler import *
import sys


if __name__ == '__main__':
    handler = DefaultExecutionHandler()
    MachineProgramExecutor.main(sys.argv, handler)
